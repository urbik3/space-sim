"use strict"

window.app = window.app || {}
window.app.game = window.app.game || {}
window.app.asteroid = window.app.asteroid || {}

/*
 * GAME
 * */
window.app.asteroid = ((asteroid, game, app, createjs, EasingFunctions) => {

    let asteroidSheets
    let asteroidTrailSheet
    let explosionSheets
    let lootSheet
    const asteroidTypes = ["grey"]

    asteroid.init = () => {
        asteroidSheets = {
            grey: [
                {
                    sheet: new createjs.SpriteSheet({
                        images: [game.assets[`asteroid_grey_1`]], 
                        frames: {width: 37, height: 38, regX: 18.5, regY: 19}
                    }),
                    radius: 7
                },
                {
                    sheet: new createjs.SpriteSheet({
                        images: [game.assets[`asteroid_grey_2`]], 
                        frames: {width: 42, height: 42, regX: 21, regY: 21}
                    }),
                    radius: 10
                },
                {
                    sheet: new createjs.SpriteSheet({
                        images: [game.assets[`asteroid_grey_3`]], 
                        frames: {width: 57, height: 50, regX: 28.5, regY: 25}
                    }),
                    radius: 17
                },
                {
                    sheet: new createjs.SpriteSheet({
                        images: [game.assets[`asteroid_grey_4`]], 
                        frames: {width: 58, height: 57, regX: 28.5, regY: 29}
                    }),
                    radius: 17
                },
                {
                    sheet: new createjs.SpriteSheet({
                        images: [game.assets[`asteroid_brown_1`]], 
                        frames: {width: 62, height: 58, regX: 31, regY: 29}
                    }),
                    radius: 19
                },
                {
                    sheet: new createjs.SpriteSheet({
                        images: [game.assets[`asteroid_grey_5`]], 
                        frames: {width: 78, height: 74, regX: 39, regY: 32}
                    }),
                    radius: 30
                },
            ]
        }

        asteroidTrailSheet = new createjs.SpriteSheet({
            images: [game.assets[`asteroid_trail`]], 
            frames: {width: 90, height: 163, regX: 45, regY: 0}
        })

        lootSheet = new createjs.SpriteSheet({
            images: [game.assets[`asteroid_loot`]], 
            frames: {width: 317, height: 170, regX: 158.5, regY: 85}
        })

        lootSheet2 = new createjs.SpriteSheet({
            images: [game.assets[`asteroid_loot2`]], 
            frames: {width: 317, height: 170, regX: 158.5, regY: 85}
        })

        explosionSheets = {
            small: new createjs.SpriteSheet({
                images: [game.assets[`explosion_small`]], 
                frames: {width: 256, height: 256, regX: 128, regY: 128, count: 64},
                animations: {
                    explode: [2, 64, null, 1]
                }
            }),
            large: new createjs.SpriteSheet({
                images: [game.assets[`explosion_large`]], 
                frames: {width: 256, height: 256, regX: 128, regY: 128, count: 64},
                animations: {
                    explode: [2, 64, null, 1]
                }
            }),
            huge: new createjs.SpriteSheet({
                images: [game.assets[`explosion_huge`]], 
                frames: {width: 256, height: 256, regX: 128, regY: 128, count: 64},
                animations: {
                    explode: [1, 64, null, 1]
                }
            }),
        }
    }
    app.addInitCallback(asteroid.init)

    asteroid.createAsteroid = (type, size, {coords = {x: 0, y: 0}, speed = {x: 0, y: 0}, scale = 1, inGroup = false}) => {
        const spriteSheet = asteroidSheets[type][size].sheet
        const hitRadius = asteroidSheets[type][size].radius
        if(!spriteSheet)
            return false
        
        const cont = new createjs.Container()
        const sprite = new createjs.Sprite(spriteSheet)
        sprite.rotation = 180

        if(coords === "random") {
            cont.x = randomInt(0, game.stageWidth)
            cont.y = randomInt(0, game.stageHeight)
        } else {
            cont.x = coords.x
            cont.y = coords.y
        }
        cont.scaleX = cont.scaleY = scale

        const lootScale = (
                size < 2 ? .25 :
                size < 5 ? .25 :
                .25) * game.scale

        const HP = (size <= 4) ? 1 : 5

        const dropLootBool = (size <= 1) ? .05 :
                    (size <= 3) ? .2 :
                    .7

        /* 
         *   TRAIL
         */
        const trailScale = (size === 0) ? .25
                            : (size === 1) ? .35
                                : (size <= 4) ? .5
                                    : .8

        const trailCont = new createjs.Container()
        trailCont.rotation = 180
        trailCont.height = asteroidTrailSheet._frameHeight * trailScale * .8

        const trailSprite = new createjs.Sprite(asteroidTrailSheet)
        trailSprite.rotation = 180
        trailSprite.scaleX = trailSprite.scaleY = trailScale

        const trailMask = new createjs.Shape()
        trailMask.graphics
            .f("#f00")
            .dr(
                -asteroidTrailSheet._frameWidth/2 * trailScale,
                0,
                asteroidTrailSheet._frameWidth * trailScale,
                asteroidTrailSheet._frameHeight * trailScale)

        // NO TRAIL
        trailMask.graphics.command.h = trailSprite.y = trailCont.height = 0

        trailSprite.mask = trailMask

        trailCont.addChild(trailSprite)

        cont.addChild(trailCont)
        cont.addChild(sprite)

        // INSTANCE
        const instance = {}

        // METHODS
        const setTrailSize = height =>
            trailMask.graphics.command.h = trailSprite.y = trailCont.height = height

        const setTrailRotation = degrees =>
            trailCont.rotation = degrees + 360/4

        const rotation = randomInt(-10, 10)/10

        const getRadius = (withScale = true) => 
            hitRadius * (withScale ? scale : 1)

        const getPosition = () =>
            game.getWorldPosition(cont)

        // LOOT
        const dropLoot = () => {
            if(randomInt(0, 10)/10 <= dropLootBool) {
                const radius = 15 * trailScale
                const lootSprite = new createjs.Sprite(size === 4 ? lootSheet2 : lootSheet)
                let pos = game.systemCont.globalToLocal(getPosition().x, getPosition().y)
                lootSprite.y = pos.y
                lootSprite.x = pos.x
                lootSprite.scaleX = lootSprite.scaleY = .01
                game.systemCont.addChild(lootSprite)
                TweenMax.to(lootSprite, .5, {scaleX: lootScale, scaleY: lootScale})
                game.addLoot({
                    lootSprite,
                    start: game.time,
                    duration: 5000,
                    _deleted: false,
                    _collected: false,
                    update: (instance, ship) => {
                        if(instance._collected)
                            return
                        // TIMEOUT
                        if(instance.start + instance.duration < game.time && !instance._deleted) {
                            instance._deleted = true
                            TweenMax.to(instance.lootSprite, .5, {
                                alpha: 0, scaleX: .5 * game.scale, scaleY: .5 * game.scale, onComplete: () => {
                                    game.systemCont.removeChild(instance.lootSprite)
                                    game.loot = game.loot.filter(loot => !loot._deleted)
                            }})
                        }
                        // COLLECT
                        if(game.distance(instance.lootSprite, ship.getPosition(true)) < radius + ship.getRadius()) {
                            instance._collected = true
                            game.gui.addLoot(size === 4 ? 5 : 1)
                            app.sound.playSFX("powerup")

                            let pos = ship.getPosition(true)
                            let vector = game.getVector(instance.lootSprite, pos)
                            let vectorLenght = game.vectorLength(vector)
                            let normalisedVector = game.normalize(vector)
                            let halfVector = {x: normalisedVector.x * vectorLenght/2, y: normalisedVector.y * vectorLenght/2}
                            TweenMax.to(instance.lootSprite, .5, {
                                alpha: 0, x: instance.lootSprite.x + halfVector.x, y: instance.lootSprite.y + halfVector.y, onComplete: () => {
                                    game.systemCont.removeChild(instance.lootSprite)
                                    instance._deleted = true
                                    game.loot = game.loot.filter(loot => !loot._deleted)
                            }})
                        }
                    }
                })
            }
        }
        
        const remove = () => {
            game.systemCont.removeChild(cont)
            instance._deleted = true
            game.asteroids = game.asteroids.filter(asteroid => !asteroid._deleted)
        }

        return Object.assign(instance, {
                cont, sprite,
                trailScale, setTrailSize, setTrailRotation,
                speed, gravity: {x: 0, y: 0}, rotation, size, HP,
                getPosition, getRadius,
                remove, inGroup, dropLoot,
                _id: ID(), _collisionTested: false, _deleted: false
            })
    }

    asteroid.createRing = (target, { asteroidType = false, asteroidSize = false,
            ringThickness = 1, asteroidSpacing = 2, distance = 10, scale = 1}) => {

        const asteroids = []
        const asteroidRing = new createjs.Container()
        target.position.addChild(asteroidRing)

        let ringDistance = target.getRadius() + distance
        const degreeRandStart = randomInt(-100, 100)/10
        let degrees = 0 + degreeRandStart

        const largestAsteroidRadius = asteroidSheets["grey"][asteroidSheets["grey"].length - 1].radius * scale

        do {
            const asteroidTypePick = !!asteroidType ? asteroidType : asteroidTypes[randomInt(0, asteroidTypes.length - 1)]
            const asteroidSizePick = randomInt(0, 1)
            const asteroidSizePx = asteroidSheets[asteroidTypePick][asteroidSizePick].radius * scale + asteroidSpacing
            const asteroidSpaceInDegrees = toDegree(asteroidSizePx / ringDistance)

            degrees += asteroidSpaceInDegrees

            let thicknessDegrees = degrees
            let prevAsteroidSizePx = asteroidSizePx
            for(let i of generateArr(1, ringThickness)) {
                const asteroidTypePick = !!asteroidType ? asteroidType : asteroidTypes[randomInt(0, asteroidTypes.length - 1)]
                const asteroidSizePick = randomInt(0, 1)
                const asteroidInstance = asteroid.createAsteroid(asteroidTypePick, asteroidSizePick, {scale, inGroup: true})
                asteroidInstance.speed.x = asteroidInstance.speed.y = 0

                const asteroidSizePx = asteroidSheets[asteroidTypePick][asteroidSizePick].radius * scale + asteroidSpacing
                const asteroidSpaceInDegrees = toDegree(asteroidSizePx / ringDistance)

                thicknessDegrees += asteroidSpaceInDegrees
                const thicknessSpacing = i * (asteroidSizePx + prevAsteroidSizePx)
                prevAsteroidSizePx = asteroidSizePx

                const asteroidRotation = new createjs.Container()
                asteroidRotation.x = asteroidRing.x
                asteroidRotation.y = asteroidRing.y
                asteroidRotation.rotation = thicknessDegrees
                asteroidRing.addChild(asteroidRotation)
    
                asteroidInstance.cont.x = ringDistance + thicknessSpacing
                asteroidInstance.cont.rotation = randomInt(0, 360)
                asteroidRotation.addChild(asteroidInstance.cont)
                asteroidInstance.remove = () => {
                    asteroidRotation.removeChild(asteroidInstance.cont)
                    asteroidInstance._deleted = true
                }
                const position = target.getPosition()
                position.x += asteroidInstance.cont.x
                asteroidInstance.getPosition = () =>
                    asteroidRotation.localToGlobal(asteroidInstance.cont.x, asteroidInstance.cont.y)

                asteroids.push(asteroidInstance)
            }

            degrees += asteroidSpaceInDegrees
        }
        while(degrees + degreeRandStart <= degreeRandStart + 360)

        // const shapeMax = new createjs.Shape()
        // shapeMax.graphics.f("rgba(255, 120, 120, .2)")
        //     .dc(0, 0, ringDistance + largestAsteroidRadius*2)
        // const shapeMin = new createjs.Shape()
        // shapeMax.graphics.f("rgba(120, 120, 250, .2)")
        //     .dc(0, 0, ringDistance + largestAsteroidRadius)
        // asteroidRing.addChild(shapeMax, shapeMin)

        const testCollision = (collisionObject) => {
            return (game.distance(collisionObject.getPosition(), target.getPosition()) - collisionObject.getRadius() < game.scale * ringDistance + largestAsteroidRadius*2)
                && (game.distance(collisionObject.getPosition(), target.getPosition()) + collisionObject.getRadius() > game.scale * ringDistance + largestAsteroidRadius)
        }

        const instance = {
            asteroids, asteroidRing,
            testCollision
        }

        target.asteroidGroups.push(instance)
        
        return instance
    }

    asteroid.explode = (coords, explosionType, asteroidSize) => {
        const sprite = new createjs.Sprite(explosionSheets[explosionType], "explode")
        // coords = game.systemCont.globalToLocal(coords.x, coords.y)
        sprite.x = coords.x
        sprite.y = coords.y
        // sprite.scaleX = sprite.scaleY = mapRange(size, 0, 1, .7, .9)
        const scale = (explosionType === "small")
                        ? ((asteroidSize < 2) ? .9 : (asteroidSize < 4) ? 1.2 : 1.5)
                        : (explosionType === "large") ? ((asteroidSize < 2) ? .8 : (asteroidSize < 4) ? 1 : 1.2)
                        : ((asteroidSize < 2) ? .8 : (asteroidSize < 4) ? 1 : 1.2)
        
        const distance = game.distance(game.shipInstance.getPosition(true), coords)
        const maxDistance = 900
        if(distance < maxDistance && explosionType === "large") {
            const volume = mapRange(distance, 100, maxDistance, .9, .2, true, EasingFunctions.easeInOutSin)
            app.sound.playSFX("asteroid_explosion_large", volume)
        }
            
        sprite.scaleX = sprite.scaleY = game.scale * scale
        sprite.rotation = randomInt(0, 360)
        game.systemCont.addChild(sprite)
    }

    asteroid.update = (deltaTime, asteroids, asteroidGroups, planets, suns, blackHoles, ship) => instance => {
        const removeTolerance = 80
        /* COLLISIONS */

        // ASTEROID COLLISIONS
        const asteroidsWithGroups = [...asteroids]
        asteroidGroups.forEach(group => {
            if(group.testCollision(instance)) {
                asteroidsWithGroups.push(...group.asteroids)
            }
        })
        asteroidsWithGroups
            .filter(asteroid => instance._id !== asteroid._id)
            .forEach(otherAsteroid => {
                if(otherAsteroid._collisionTested)
                    return
                
                if(otherAsteroid.getRadius() + instance.getRadius() >= game.distance(instance.getPosition(), otherAsteroid.getPosition())) {
                    // const position = !!otherAsteroid.inGroup
                    //     ? game.systemCont.globalToLocal(otherAsteroid.getPosition().x, otherAsteroid.getPosition().y)
                    //     : {x: (instance.getPosition().x + otherAsteroid.getPosition().x) / 2, y: (instance.getPosition().y + otherAsteroid.getPosition().y) / 2}
                    let position = {x: (instance.getPosition().x + otherAsteroid.getPosition().x) / 2, y: (instance.getPosition().y + otherAsteroid.getPosition().y) / 2}
                    position = game.systemCont.globalToLocal(position.x, position.y)
                    asteroid.explode(position, "large", Math.round((instance.size + otherAsteroid.size) / 2))
                    instance.HP -= 1
                    otherAsteroid.HP -= 1
                    if(instance.HP <= 0)
                        instance.remove()
                    if(otherAsteroid.HP <= 0)
                        otherAsteroid.remove()
                }
            })

        // PLANET COLLISIONS
        const planetsAndSuns = [...planets, ...suns]
        planetsAndSuns.forEach(planet => {
            if(planet.getRadius() + instance.getRadius() >= game.distance(instance.getPosition(), planet.getPosition())) {
                planet.collisionExplosion(game.getAngle(planet.getPosition(), instance.getPosition(), true), (instance.size+1) / asteroidSheets.grey.length)
                planet.getHit(1)
                instance.remove()
            }
        })
        instance._collisionTested = true

        // SHIP COLLISION
        if(ship.getRadius() + instance.getRadius() >= game.distance(instance.getPosition(), ship.getPosition())) {
            ship.activateShield(game.getAngle(ship.getPosition(true), instance.getPosition()))
            const position = game.systemCont.globalToLocal(instance.getPosition().x, instance.getPosition().y)
            asteroid.explode(position, "large", Math.max(instance.size-1, 0))
            instance.remove()
        }

        /* MOVEMENT */

        // GRAVITY
        const planetAndSunsAndBlackHoles = [...planetsAndSuns, ...blackHoles]
        planetAndSunsAndBlackHoles.forEach(object => {
            const planetPos = object.getPosition()
            const asteroidPos = instance.getPosition()
            const distance = (
                game.distance(planetPos, asteroidPos)
                    - instance.getRadius()
                    - object.getRadius()
                )

            if(distance < object.gravityDistance) {
                const gravityVectorLength = mapRange(distance, 0, object.gravityDistance, object.gravityAcceleration, 0, true, EasingFunctions.easeInQuad)/100
                const gravityVectorNorm = game.normalize(game.getVector(asteroidPos, planetPos))
                const gravityVector = {x: gravityVectorLength * gravityVectorNorm.x, y: gravityVectorLength * gravityVectorNorm.y}
                instance.speed.x += gravityVector.x
                instance.speed.y += gravityVector.y
            }
        })

        // MOVING
        instance.cont.x += instance.speed.x
        instance.cont.y += instance.speed.y
        instance.sprite.rotation += instance.rotation

        // TRAIL
        instance.setTrailRotation(
            game.getAngle({x: 0, y: 0}, instance.speed, true))
        
        const maxTrailSize = asteroidTrailSheet._frameHeight * instance.trailScale * .8
        if(game.vectorLength(instance.speed) > .1)
            instance.setTrailSize(
                Math.min(mapRange(game.vectorLength(instance.speed), 0, 1.8, 30 * instance.trailScale, maxTrailSize), maxTrailSize))

                
        if(instance.cont.x < 0 - removeTolerance || instance.cont.x > game.stageWidth + removeTolerance
            || instance.cont.y < 0 - removeTolerance || instance.cont.y > game.stageHeight + removeTolerance)
                instance.remove()
    }

    /*
    * END
    * */
    return asteroid
})(window.app.asteroid, window.app.game, window.app, createjs, EasingFunctions)