"use strict"

window.app = window.app || {}
window.app.sound = window.app.sound || {}
window.app.game = window.app.game || {}

/*
 * DOUBLE RANDOM
 * */
window.app.sound = ((sound, app, game, $, createjs, EasingFunctions) => {

    sound.playing

    sound.playMusic = id => {
        if(sound.playing)
            TweenMax.to(sound.playing, 1, {volume: 0, onComplete: sound.playing.stop})
        sound.playing = createjs.Sound.play(id)
        sound.playing.loop = -1
        sound.playing.volume = 1
    }

    sound.playSFX = (id, volume = .8) => {
        const sound = createjs.Sound.play(id)
        sound.volume = volume
        return sound
    }

    return sound

})(window.app.sound, window.app, window.app.game, jQuery, createjs, EasingFunctions)