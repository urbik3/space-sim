"use strict"

window.app = window.app || {}
window.app.game = window.app.game || {}
window.app.game.gui = window.app.game.gui || {}
window.app.solarSystem = window.app.boss || {}
window.app.asteroid = window.app.asteroid || {}
window.app.ship = window.app.ship || {}

/*
 * GAME
 * */
window.app.game = ((game, app, createjs, EasingFunctions) => {

    game.levelNumber
    game.solarSystems = []
    game.blackHoles = []
    game.asteroids = []
    game.asteroidGroups = []
    game.shipInstance = null
    game.projectiles = []
    game.loot = []

    game.time = 0
    game.startTime = 0
    game.pauseTimeStart = 0
    game.pausedTime = 0
    game.scale = .6 //randomInt(60, 90)/100

    game.getTime = () => (game.time - game.startTime) / 1000

    game.STATUS_PLAYING = "playing"
    game.STATUS_PAUSED = "paused"
    game.STATUS_ENDGAME = "endgame"
    game.status = game.STATUS_PLAYING
    
    const controls = e => {
        if(e.keyCode === 27 || e.keyCode === 32) {
            if(game.status === game.STATUS_PAUSED || game.status === game.STATUS_PLAYING) {
                game.status = (game.status === game.STATUS_PLAYING)
                    ? game.STATUS_PAUSED : game.STATUS_PLAYING
                
                if(game.status === game.STATUS_PAUSED) {
                    app.screenManager.showPauseMenu()
                } else if(game.status === game.STATUS_PLAYING) {
                    app.screenManager.hidePauseMenu()
                }
            }
        }
    }
    game.controls = controls

    game.precisionMode = false
    const ctrHold = (e) => {
        // game.precisionMode = e.ctrlKey
    }

    game.init = (levelNumber = 1) => {
        game.time = game.startTime = Date.now()
        game.levelNumber = levelNumber

        // GAME STAGE
        game.status = game.STATUS_PLAYING
        if(game.stage)
            app.stage.removeChild(game.stage)
        game.stage = new createjs.Container()
        game.stage.z = 1
        game.projectilesCont = new createjs.Container()
        game.projectilesCont.z = 20
        game.systemCont = new createjs.Container()
        game.systemCont.z = 0
        app.stage.addChild(game.stage)
        game.stage.addChild(game.systemCont, game.projectilesCont)

        game.scale = app.levelManager[`config${levelNumber}`]().scale
        game.stageWidth = app.levelManager[`config${levelNumber}`]().width
        game.stageHeight = app.levelManager[`config${levelNumber}`]().height
        game.mapPadding = app.levelManager[`config${levelNumber}`]().mapPadding / game.scale
        app.sound.playMusic(app.levelManager[`config${levelNumber}`]().music)

        game.clearStage()
        document.addEventListener("keydown", controls)

        app.bg[`init${levelNumber}`]()
        game.stage.addChild(game.crosshairSprite)
        app.game.gui.init()
        app.levelManager[`init${levelNumber}`]()
        game.sortGfx(game.stage)
    }

    game.clearStage = () => {
        game.solarSystems = []
        game.blackHoles = []
        game.asteroids = []
        game.asteroidGroups = []
        if(game.shipInstance)
            game.shipInstance._deleted = true
        game.shipInstance = null
        game.projectiles = []
        app.levelManager.actions = []
        app.game.gui.reset()

        document.removeEventListener("keydown", controls)
    }

    game.resize = () => {

    }

    game.update = (deltaTime) => {
        if(game.status !== game.STATUS_PLAYING)
            return
        
        game.time += deltaTime

        game.solarSystems.forEach(system =>
            system.planets = system.planets.filter(planet => !planet._destroyed))
        game.solarSystems.forEach(app.solarSystem.update(deltaTime))

        const planets = game.solarSystems.reduce((planets, system) => {
            planets.push(...system.planets)
            return planets
        }, [])

        const suns = game.solarSystems.reduce((suns, system) => {
            suns.push(system.sun)
            return suns
        }, [])

        game.loot.forEach(loot => loot.update(loot, game.shipInstance))
        game.projectiles.forEach(app.ship.projectileUpdate(deltaTime, planets, suns, game.asteroids, game.asteroidGroups))

        game.asteroids.forEach(asteroid => asteroid._collisionTested = false)
        game.asteroids.forEach(app.asteroid.update(deltaTime, game.asteroids, game.asteroidGroups, planets, suns, game.blackHoles, game.shipInstance))

        game.blackHoles.forEach(instance => app.blackHole.update(instance))

        app.ship.update(deltaTime, game.shipInstance, planets, suns, game.asteroidGroups, game.blackHoles)

        game.asteroidGroups.forEach(group => 
            group.asteroids = group.asteroids.filter(asteroid => !asteroid._deleted))

        // ACTIONS
        app.levelManager.actions.sort((x, y) => {
            if (x[0] < y[0])
                return -1
            if (x[0] > y[0])
                return 1
            return 0
        })

        const winGame = () => {
            const score = app.game.gui.getScore()
            app.gameManager.game.levels[game.levelNumber-1].won = true

            if(app.gameManager.game.levels[game.levelNumber] && app.gameManager.game.levels[game.levelNumber].available === false)
                app.gameManager.game.levels[game.levelNumber].available = true
            if(app.gameManager.game.levels[game.levelNumber-1 + app.levelManager.levelRows] && app.gameManager.game.levels[game.levelNumber-1 + app.levelManager.levelRows].available === false)
                app.gameManager.game.levels[game.levelNumber-1 + app.levelManager.levelRows].available = true

            game.status = game.STATUS_ENDGAME
            app.screenManager.showEndgame(false)

            if(app.gameManager.game.levels[game.levelNumber-1].score < score)
                app.gameManager.game.levels[game.levelNumber-1].score = score
            app.gameManager.save()
        }

        while(app.levelManager.actions[0] && app.levelManager.actions[0][0] <= game.getTime()) {
            let params = app.levelManager.actions.shift()
            if(params[1] === app.levelManager.ACTION_METEOR_SHOWER) {
                params.shift()
                params.shift()
                let asteroid = app.asteroid.createAsteroid(...params)
                game.systemCont.addChild(asteroid.cont)
                game.asteroids.push(asteroid)
            } else if(params[1] === app.levelManager.ACTION_WIN) {
                winGame()
            }
        }

        if(app.levelManager.actions.length <= 0 && game.asteroids.length <= 0) {
            winGame()
        }

        app.bg.update(deltaTime, game.shipInstance)
        app.game.gui.update(deltaTime)
        game.sortGfx(game.stage)
    }

    game.addProjectile = projectile => {
        game.projectiles.push(projectile)
    }

    game.addLoot = loot => {
        game.loot.push(loot)
    }

    game.assets = {}
    game.preload = () => {
        let assets = [
            {name: "shadow", src: "solar-system/PLanet_Shadow_1.png"},
            {name: "sun", src: "solar-system/Sun.png"},
            {name: "asteroid_trail", src: "asteroid/asteroid-trail.png"},
            {name: "whirl", src: "whirl.png"},
            {name: "asteroid_loot", src: "asteroid/loot.png"},
            {name: "asteroid_loot2", src: "asteroid/loot2.png"},
            {name: "crosshair", src: "crosshair.png"},

            {name: "explosion_hit_planet", src: "explosions/2.png"},
            {name: "explosion_destroy_planet", src: "explosions/4_large.png"},
            {name: "explosion_destroy_ship", src: "explosions/4.png"},
            {name: "explosion_small", src: "explosions/1.png"},
            {name: "explosion_large", src: "explosions/3.png"},
            {name: "explosion_huge", src: "explosions/5.png"},

            {name: "ship_1", src: "ship/ship-1.png"},
            {name: "ship_1_jet", src: "ship/ship-1-jet.png"},
            {name: "ship_1_turret", src: "ship/ship-1-turret.png"},
            {name: "ship_1_turret_laser", src: "ship/ship-1-laser.png"},
            {name: "gunfire_1", src: "gunfire-1.png"},
            {name: "gunfire_2", src: "gunfire-2.png"},
            {name: "ship_shield", src: "spr_shield.png"},

            // BG
            {name: "starfield_small", src: "bg/starfield-small.png"},
            {name: "starfield_medium", src: "bg/starfield-medium.png"},
            {name: "starfield_big", src: "bg/starfield-big.png"},
            {name: "nebulae_hot", src: "bg/nebula-hot.png"},
            {name: "nebulae_cold", src: "bg/nebula-cold.png"},
            {name: "dust_blue", src: "bg/dust-blue.png"},
            {name: "dust_violet", src: "bg/dust-violet.png"},
            {name: "dust_yellow", src: "bg/dust-yellow.png"},
            {name: "asteroid_dust_1", src: "asteroid-dust-1.png"},
            {name: "asteroid_dust_2", src: "asteroid-dust-2.png"},
        ]
            .concat(
                generateArr(0, 9)
                    .map(i => ({name: `planet${i}`, src: `solar-system/Planet${i}.png`})))
            .concat(
                generateArr(1, 5)
                    .map(i => ({name: `asteroid_grey_${i}`, src: `asteroid/asteroid-grey-${i}.png`})))
            .concat(
                generateArr(1, 1)
                    .map(i => ({name: `asteroid_brown_${i}`, src: `asteroid/asteroid-brown-${i}.png`})))
        let promises = assets
            .map(texture => {
                return new Promise((resolve, reject) => {
                    game.assets[texture.name] = new Image();
                    game.assets[texture.name].onload = game.handleImageLoad(resolve);
                    game.assets[texture.name].onerror = game.handleImageError(reject);
                    game.assets[texture.name].src = `assets/${texture.src}`;
                    // console.log(texture.name, game.assets[texture.name])
                })
            })

        const path = ""
        createjs.Sound.alternateExtensions = ["ogg"]
        const soundAssets = [
            {src: path+"assets/sound/sfx/asteroid_destroy.mp3", name: "asteroid_destroy"},
            {src: path+"assets/sound/sfx/asteroid_explosion_large.mp3", name: "asteroid_explosion_large"},
            {src: path+"assets/sound/sfx/big_explosion.mp3", name: "big_explosion"},
            {src: path+"assets/sound/sfx/blaster.mp3", name: "blaster"},
            {src: path+"assets/sound/sfx/powerup.mp3", name: "powerup"},
            {src: path+"assets/sound/sfx/btn_select.mp3", name: "btn_select"},
            {src: path+"assets/sound/sfx/btn_hover.mp3", name: "btn_hover"},
            
            {src: path+"assets/sound/OutThere.mp3", name: "main_music"},
            {src: path+"assets/sound/Voyager.mp3", name: "musicVoyager"},
            {src: path+"assets/sound/Propaganda.mp3", name: "musicPropaganda"},
            {src: path+"assets/sound/Cargoship.mp3", name: "musicCargoship"},
            {src: path+"assets/sound/BattleInTheMarket.mp3", name: "musicBattleInTheMarket"}
        ]
        const soundsPromise = new Promise((resolve, reject) => {
            let loadedSoundsNum = 0
            createjs.Sound.on("fileload", () => {
                console.log("sound loaded")
                loadedSoundsNum++
                if(loadedSoundsNum >= soundAssets.length - 10)
                    resolve()
            }, this)
        })
        soundAssets.forEach(sound => createjs.Sound.registerSound(sound.src, sound.name))

        promises.push(soundsPromise)
        return Promise.all(promises)
    }
    
    game.handleImageLoad = cb => () => {
        console.log("loaded")
        cb()
    }

    game.handleImageError = cb => () => {
        console.log("error")
        cb()
    }

    /*
    * HELP FUNCTIONS
    * */

    // GAME OBJECTS
    game.getGameObjects = () => gameObjects
    game.addGameObject = (...args) => gameObjects.push(...args)
    game.removeGameObject = ID => gameObjects.splice(gameObjects.findIndex(go=>go.ID===ID), 1)

    // POSITION
    game.moveTo = (graphics, x, y) => {
        graphics.x = x
        graphics.y = y
    }

    game.translate = (graphics, x, y) => {
        graphics.x += x
        graphics.y += y
    }

    game.getWorldPosition = obj =>
        obj.localToGlobal(0, 0)

    // COLLISIONS
    const getIntersectionPoints = (x, y, radius, gameObject) => {
        const
            worldPos = game.getWorldPosition(gameObject.gfx),
            centerDistance = game.distance({x, y}, worldPos),
            pointVDistance = (radius**2 - gameObject.config.radius**2 + centerDistance**2) / (centerDistance*2),
            vToIntersectionDistance = Math.sqrt(radius**2 - pointVDistance**2),
            pointV = {
                x: x + pointVDistance * (worldPos.x - x) / centerDistance,
                y: y + pointVDistance * (worldPos.y - y) / centerDistance
            },
            IS1 = {
                x: pointV.x + vToIntersectionDistance * (y - worldPos.y) / centerDistance,
                y: pointV.y - vToIntersectionDistance * (x - worldPos.x) / centerDistance
            },
            IS2 = {
                x: pointV.x - vToIntersectionDistance * (y - worldPos.y) / centerDistance,
                y: pointV.y + vToIntersectionDistance * (x - worldPos.x) / centerDistance
            }

        return {IS1, IS2}
    }

    game.collisionCircles = (x, y, radius, ID = null) =>
        gameObjects
            .filter(o=>o.ID!==ID)
            .filter(o=>o.gfx)
            .filter(o => {
                const worldPos = game.getWorldPosition(o.gfx)
                return game.distance({x, y}, worldPos) < radius+o.config.radius
            })
            .map(gameObject => ({
                gameObject,
                intersections: getIntersectionPoints(x, y, radius, gameObject)
            }))

    game.collisionArcToCircles = (x, y, radius, rotation, hitAngle, ID = null) =>
        game
            .collisionCircles(x, y, radius, ID)
            .map(collision => {
                ;({IS1, IS2} = collision.intersections)

                // intersection points inside arc
                const
                    angle1 = game.getAngle({x, y}, IS1),
                    angle2 = game.getAngle({x, y}, IS2),
                    maxAngle = rotation+hitAngle,
                    minAngle = rotation-hitAngle,
                    hitIS1 = radBetween(angle1, minAngle, maxAngle),
                    hitIS2 = radBetween(angle2, minAngle, maxAngle)

                // // arc inside circle
                // const
                //     angleSwitch = Math.abs(angle1 - angle2) > Math.PI,
                //     angleBottom = angle1 < angle2 ? (angleSwitch?angle2:angle1) : (angleSwitch?angle1:angle2),
                //     angleTop = angle1 > angle2 ? (angleSwitch?angle2:angle1) : (angleSwitch?angle1:angle2),
                //     arcInsideCircle = radBetween(rotation, angleBottom, angleTop)

                // return hitIS1 || hitIS2 || arcInsideCircle

                let intersections = {}
                if(hitIS1)
                    intersections.IS1 = IS1
                if(hitIS2)
                    intersections.IS2 = IS2

                collision.intersections = intersections

                return collision
            })
            .filter(c => c.intersections.IS1 || c.intersections.IS2)
    
    game.getHitPosition = (intersections, hitObject) => {
        if(!intersections.IS2 && !intersections.IS1)
            return false

        if(!intersections.IS1 && intersections.IS2)
            return intersections.IS2
        else if(!intersections.IS2 && intersections.IS1)
            return intersections.IS1

        const
            IS1angle = app.game.getAngle(hitObject.gfx, intersections.IS1),
            IS2angle = app.game.getAngle(hitObject.gfx, intersections.IS2),
            isIS1Higher = rad(IS1angle - IS2angle) < Math.PI,
            lowerAngle = isIS1Higher ? IS2angle : IS1angle,
            higherAngle = isIS1Higher ? IS1angle : IS2angle,
            diffAngle = rad(higherAngle - lowerAngle),
            middleAngle = lowerAngle + diffAngle/2,
            relativeHitPosition = {x: hitObject.config.radius * Math.cos(middleAngle), y: hitObject.config.radius * Math.sin(middleAngle)},
            hitPosition = {x: relativeHitPosition.x + hitObject.gfx.x, y: relativeHitPosition.y + hitObject.gfx.y}

        return hitPosition
    }

    // MATH
    game.distance = ({x:x1, y:y1}, {x:x2, y:y2}) =>
        Math.sqrt((x1-x2)**2+(y1-y2)**2)

    game.getVector = ({x:x1, y:y1}, {x:x2, y:y2}) =>
        ({x: (x2-x1), y: (y2-y1)})

    game.addVector = ({x:x1, y:y1}, {x:x2, y:y2}) =>
        ({x: (x2+x1), y: (y2+y1)})

    game.vectorLength = (coords) =>
        game.distance({x: 0, y: 0}, coords)

    game.normalize = ({x, y}) => {
        const length = Math.sqrt(x**2 + y**2)
        if(length <= 0)
            return {x: 0, y: 0}
        return {x: x / length, y: y / length}
    }

    game.polarToCartesian = (r, angle) => ({x: r * Math.cos(angle), y: r * Math.sin(angle)})

    // @sectorAngle: angle between origin, target and origin x+ (point on the right of origin)
    game.getAngle = (coordsOrigin, coordsTarget, degree = false) => {
        const
            sector = (coordsOrigin.x > coordsTarget.x ? "left" : "right") + (coordsOrigin.y > coordsTarget.y ? "top" : "bottom"),
            sideX = Math.abs(coordsOrigin.x - coordsTarget.x),
            sideY = Math.abs(coordsOrigin.y - coordsTarget.y),
            sectorAngle = Math.atan(sideY/sideX)

        let angle =
            sector === "righttop" ? -sectorAngle + Math.PI*2 :
                sector === "rightbottom" ? sectorAngle :
                    sector === "leftbottom" ? -sectorAngle + Math.PI :
                        sector === "lefttop" ? sectorAngle + Math.PI :
                            NaN
        if(degree)
            angle = toDegree(angle)

        return angle
    }

    game.getVectorAngle = (vectorCoords, degree = false) =>
        game.getAngle({x: 0, y: 0}, vectorCoords, degree)

    // OTHER
    const depthCompare = (a, b) => {
        if(a.z < b.z)
            return -1
        if(a.z > b.z)
            return 1
        return 0
    }
    game.sortGfx = (gfx) =>
        gfx.sortChildren(depthCompare)

    /*
    * END
    * */
    return game
})(window.app.game, window.app, createjs, EasingFunctions)