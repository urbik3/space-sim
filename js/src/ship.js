"use strict"

window.app = window.app || {}
window.app.game = window.app.game || {}
window.app.ship = window.app.ship || {}

/*
 * GAME
 * */
window.app.ship = ((ship, game, app, createjs, EasingFunctions) => {

    ship.gfx = {}

    ship.init = () => {
        // ship.gfx.blasterSheet = new createjs.SpriteSheet({
        //     images: [game.assets[`gunfire_1`]], 
        //     frames: {width: 246, height: 360, regX: 123, regY: 120}
        // })
        ship.gfx.blasterSheet = new createjs.SpriteSheet({
            images: [game.assets[`gunfire_2`]], 
            frames: {width: 259, height: 329, regX: 136, regY: 66}
        })
        ship.gfx.shieldSheet = new createjs.SpriteSheet({
            images: [game.assets[`ship_shield`]], 
            frames: {width: 556, height: 556, regX: 278, regY: 278}
        })
    }
    app.addInitCallback(ship.init)

    ship.create = ({startX = 50, startY = game.stageHeight/2}) => {
        // GFX
        const shipSprite = new createjs.Sprite(
            new createjs.SpriteSheet({
                images: [game.assets[`ship_1`]], 
                frames: {width: 89, height: 89, regX: 45, regY: 69}
        }))
        const jetSprite = new createjs.Sprite(
            new createjs.SpriteSheet({
                images: [game.assets[`ship_1_jet`]], 
                frames: {width: 45, height: 82, regX: 22.5, regY: 0}
        }))
        const turretSprite = new createjs.Sprite(
            new createjs.SpriteSheet({
                images: [game.assets[`ship_1_turret`]], 
                frames: {width: 29, height: 62, regX: 15, regY: 43}
        }))
        const turretLaserSprite = new createjs.Sprite(
            new createjs.SpriteSheet({
                images: [game.assets[`ship_1_turret_laser`]], 
                frames: {width: 38, height: 400, regX: 19, regY: 0}
        }))
        turretLaserSprite.rotation = -90
        if(app.gameManager.ship.projectileMaxDistance === app.gameManager.upgrades.projectileMaxDistance[0]) {
            turretLaserSprite.x = -150
            turretLaserSprite.cache(-19, 150, 38, 250)
        } else if(app.gameManager.ship.projectileMaxDistance === app.gameManager.upgrades.projectileMaxDistance[1]) {
            turretLaserSprite.x = -100
            turretLaserSprite.cache(-19, 100, 38, 300)
        }

        const shipCenter = {x: 20 * game.scale, y: 0}
        shipSprite.rotation = 90
        turretSprite.rotation = 90

        const shipCont =  new createjs.Container()
        shipCont.z = 10
        const shipTransform =  new createjs.Container()
        const turretTransform =  new createjs.Container()
        const jetTransform =  new createjs.Container()

        // JET
        const jetCont = new createjs.Container()
        jetCont.height = jetSprite.spriteSheet._frameHeight
        jetCont.y = 19
        jetTransform.rotation = 90

        const jetMask = new createjs.Shape()
        jetMask.graphics
            .f("#f00")
            .dr(
                -jetSprite.spriteSheet._frameWidth/2,
                0,
                jetSprite.spriteSheet._frameWidth,
                jetSprite.spriteSheet._frameHeight)

        jetSprite.mask = jetMask
        jetCont.addChild(jetSprite)

        // COMPOSITE
        shipTransform.addChild(shipSprite)
        shipTransform.addChild(jetTransform)
        shipTransform.addChild(turretTransform)
        jetTransform.addChild(jetCont)
        if(app.gameManager.ship.laser)
            turretTransform.addChild(turretLaserSprite)
        turretTransform.addChild(turretSprite)
        shipCont.addChild(shipTransform)
        game.systemCont.addChild(shipCont)

        shipCont.x = startX
        shipCont.y = startY
        shipTransform.scaleX = shipTransform.scaleY = game.scale

        // OPTIONS
        const hp = app.gameManager.ship.hp
        app.game.gui.setShipMaxHP(hp)
        app.game.gui.setShipHP(hp)
        game.precisionMode = app.gameManager.ship.precisionMode

        // METHODS
        const setJetSize = pos =>
            jetSprite.y = pos

        const getPosition = (systemLocalPosition = false) =>
            systemLocalPosition
                ? shipCont.localToLocal(shipCenter.x, shipCenter.y, game.systemCont)
                : shipCont.localToGlobal(shipCenter.x, shipCenter.y)

        const getRadius = (withScale = true) => 
            Math.max(shipSprite.spriteSheet._frameWidth, shipSprite.spriteSheet._frameHeight) * .75 * (withScale ? game.scale : 1)

        const activateShield = (radians, removeAfter = true, damage = 1) => {
            const sprite = new createjs.Sprite(ship.gfx.shieldSheet)
            sprite.rotation = deg(toDegree(radians - toRad(shipCont.rotation)))
            sprite.scaleX = sprite.scaleY = game.scale * .25
            sprite.x = shipCenter.x
            sprite.y = shipCenter.y
            shipCont.addChild(sprite)
            if(removeAfter)
                setTimeout(() => {
                    TweenMax.to(sprite, .5, {alpha: 0, onComplete: () => shipCont.removeChild(sprite)})
                }, 500)

            generateArr(1, damage)
                .forEach(() => {
                    if(instance.HPBars.length >= 1) {
                        const hpBar = instance.HPBars.pop()
                        TweenMax.to(hpBar, .75, {scaleX: .7, scaleY: .7, alpha: .2})
                        instance.hp -= 1
                    }
                })
            app.game.gui.setShipHP(instance.hp)
            if(instance.hp <= 0 && instance._destroyAnimationPlaying !== true) {
                instance.destroy()
            }
        }

        const destroy = () => {
            const sprite = new createjs.Sprite(new createjs.SpriteSheet({
                images: [game.assets[`explosion_destroy_ship`]], 
                frames: {width: 256, height: 256, regX: 128, regY: 128, count: 64},
                animations: {
                    explode: [0, 64, null, 1]
                }
            }), "explode")
            sprite.x = shipCenter.x
            sprite.y = shipCenter.y
            sprite.scaleX = sprite.scaleY = .8
            sprite.rotation = randomInt(0, 360)
            shipCont.addChild(sprite)
            
            instance._destroyAnimationPlaying = true
            app.sound.playSFX("big_explosion")
            
            sprite.addEventListener("change", (e, type) => {
                const destroySpriteFrame = 18
                if(e.currentTarget.currentFrame >= destroySpriteFrame) {
                    shipCont.removeChild(shipTransform)
                }
                if(e.currentTarget.currentFrame >= 64) {
                    instance._destroyed = true
                    game.status = game.STATUS_ENDGAME
                    app.screenManager.showEndgame(true, "Your ship has been destroyed")
                }
            })
        }

        // if(true) { //show hit radius
        //     const gfx = new createjs.Shape()
        //     gfx.graphics
        //         .f("#f00")
        //         .dc(0, 0, getRadius())
        //     gfx.x = shipCenter.x
        //     gfx.y = shipCenter.y
        //     gfx.alpha = .25
        //     shipCont.addChild(gfx)
        // }

        // INSTANCE
        let instance = {
            getPosition,
            getRadius,
            setJetSize,
            activateShield,
            hp,
            destroy,
            _destroyAnimationPlaying: false,
            gfx: {
                shipCont,
                shipTransform,
                shipSprite,
                turretSprite,
                turretTransform,
                jetSprite,
                jetCont,
            },
            movement: {
                rotation: 0,
                speed: {x: 0, y: 0},
                thrust: false, back: false, left: false, right: false
            }
        }

        instance = game.gui.renderShipHitGUI(instance)
        instance.HPBarPosition.x = 42
        instance.HPBarBg.x = 30 + (instance.hp * (game.gui.config.planetHPBarWidth + game.gui.config.planetHPBarPadding) / 2)
        game.stage.addChild(instance.HPBarCont)

        instance.setJetSize(0)
        addControl(instance)

        return instance
    }

    ship.update = (deltaTime, instance, planets, suns, asteroidGroups, blackHoles) => {
        // SETTINGS
        const rotationAcc = app.gameManager.ship.shipRotationAcc
        const maxRotationSpeed = app.gameManager.ship.shipRotationSpeed
        const maxTurretRotation = app.gameManager.ship.turretRotation

        const maxThrustSpeed = app.gameManager.ship.speedMax * game.scale
        const maxOtherSpeed = 5 * game.scale
        const thrustSpeed = app.gameManager.ship.speedAcc * game.scale
        const otherSpeed = .1 * game.scale
        const friction = .05 * game.scale

        // SHIP ROTATION
        const lookAt = {x: app.stage.mouseX, y: app.stage.mouseY}
        const rotateTo = game.getAngle(instance.getPosition(), lookAt, true)
        const rotationSide = rotateTo < instance.gfx.shipCont.rotation
                                ? (Math.abs(rotateTo - instance.gfx.shipCont.rotation) > 180
                                    ? 1 : -1)
                                : (Math.abs(rotateTo - instance.gfx.shipCont.rotation) > 180
                                    ? -1 : 1)

        if(!instance._destroyAnimationPlaying) {          
            if(Math.abs(instance.gfx.shipCont.rotation - rotateTo) > maxRotationSpeed)
                instance.movement.rotation += rotationAcc
            else
                instance.movement.rotation = 0

            instance.movement.rotation = Math.min(instance.movement.rotation, maxRotationSpeed)
            
            instance.gfx.shipCont.rotation = deg(instance.gfx.shipCont.rotation + instance.movement.rotation * rotationSide)
        }

        // TURRET ROTATION
        const delta = 80
        let turrentRotation = rotateTo - instance.gfx.shipCont.rotation
        if(rotateTo < delta && instance.gfx.shipCont.rotation > 360 - delta) {
            turrentRotation = 360 - instance.gfx.shipCont.rotation + rotateTo
        } else if(rotateTo > 360 - delta && instance.gfx.shipCont.rotation < delta) {
            turrentRotation = -(360 - rotateTo + instance.gfx.shipCont.rotation)
        }
        turrentRotation = Math.max(Math.min(turrentRotation, maxTurretRotation), -maxTurretRotation)
        instance.gfx.turretTransform.rotation = deg(turrentRotation)

        // JET SIZE
        const jetSize = !instance.movement.thrust
            ? Math.max(instance.gfx.jetSprite.y - 5, -48)
            : Math.min(instance.gfx.jetSprite.y + 5, -16)
        instance.setJetSize(jetSize)
        
        // THRUST
        if(instance.movement.thrust) {
            const thrustVector = game.polarToCartesian(thrustSpeed, toRad(instance.gfx.shipCont.rotation))
            instance.movement.speed.x += thrustVector.x
            instance.movement.speed.y += thrustVector.y
        }
        if(game.vectorLength(instance.movement.speed) <= maxOtherSpeed) {
            if(instance.movement.back) {
                const thrustVector = game.polarToCartesian(otherSpeed, toRad(instance.gfx.shipCont.rotation-180))
                instance.movement.speed.x += thrustVector.x
                instance.movement.speed.y += thrustVector.y
            }
            if(instance.movement.left) {
                const thrustVector = game.polarToCartesian(otherSpeed, toRad(instance.gfx.shipCont.rotation-90))
                instance.movement.speed.x += thrustVector.x
                instance.movement.speed.y += thrustVector.y
            }
            if(instance.movement.right) {
                const thrustVector = game.polarToCartesian(otherSpeed, toRad(instance.gfx.shipCont.rotation-270))
                instance.movement.speed.x += thrustVector.x
                instance.movement.speed.y += thrustVector.y
            }
        }

        if(game.vectorLength(instance.movement.speed) > maxThrustSpeed) {
            instance.movement.speed = game.normalize(instance.movement.speed)
            instance.movement.speed.x *= maxThrustSpeed
            instance.movement.speed.y *= maxThrustSpeed
        } else {
            const speedVectorLength = game.vectorLength(instance.movement.speed)
            const speedVectorLengthWithFriction = Math.max(speedVectorLength - friction, 0)
            instance.movement.speed = game.normalize(instance.movement.speed)
            instance.movement.speed.x *= speedVectorLengthWithFriction
            instance.movement.speed.y *= speedVectorLengthWithFriction
        }

        blackHoles.forEach(blackHole => {
            const blackHolePos = blackHole.getPosition()
            const shipPos = instance.getPosition()
            const distance = (
                game.distance(blackHolePos, shipPos)
                    - instance.getRadius()
                    - blackHole.getRadius()
                )

            if(distance < blackHole.gravityDistance) {
                const gravityVectorLength = mapRange(distance, 0, blackHole.gravityDistance, blackHole.gravityAcceleration, 0, true, EasingFunctions.easeInQuad)/10
                const gravityVectorNorm = game.normalize(game.getVector(shipPos, blackHolePos))
                const gravityVector = {x: gravityVectorLength * gravityVectorNorm.x, y: gravityVectorLength * gravityVectorNorm.y}
                instance.movement.speed.x += gravityVector.x
                instance.movement.speed.y += gravityVector.y
            }
        })
        
        instance.gfx.shipCont.x += instance.movement.speed.x
        instance.gfx.shipCont.y += instance.movement.speed.y

        instance.gfx.shipCont.x = Math.min(instance.gfx.shipCont.x, game.stageWidth - instance.getRadius())
        instance.gfx.shipCont.y = Math.min(instance.gfx.shipCont.y, game.stageHeight - instance.getRadius())
        instance.gfx.shipCont.x = Math.max(instance.gfx.shipCont.x, instance.getRadius())
        instance.gfx.shipCont.y = Math.max(instance.gfx.shipCont.y, instance.getRadius())

        // COLLISIONS
        const planetsAndSuns = [...planets, ...suns]
        planetsAndSuns.forEach(planet => {
            if(planet.getRadius() + instance.getRadius() >= game.distance(instance.getPosition(), planet.getPosition())) {
                const
                    planetPosition = planet.getPosition(),
                    vector = app.game.getVector(planetPosition, instance.getPosition()),
                    distance = app.game.distance(planetPosition, instance.getPosition()),
                    pushDistance = Math.abs(distance - instance.getRadius() - planet.getRadius()),
                    vectorMultiplier = pushDistance/distance,
                    pushVector = {
                        x: vector.x * vectorMultiplier,
                        y: vector.y * vectorMultiplier
                    },
                    shipPosition = app.game.addVector(instance.gfx.shipCont, pushVector)
                instance.gfx.shipCont.x = shipPosition.x
                instance.gfx.shipCont.y = shipPosition.y

                const
                    normalisedVector = game.normalize(vector),
                    impactForceStrength = 4 * game.scale,
                    impactForceVector = {
                        x: normalisedVector.x * impactForceStrength,
                        y: normalisedVector.y * impactForceStrength
                    },
                    
                    shipSpeedVectorLength = game.vectorLength(instance.movement.speed),
                    shipSpeedMultiplier = .2,
                    shipSpeedVectorNormalized = game.normalize(instance.movement.speed),
                    shipNewSpeedVector = {
                        x: shipSpeedVectorNormalized.x * shipSpeedVectorLength * shipSpeedMultiplier,
                        y: shipSpeedVectorNormalized.y * shipSpeedVectorLength * shipSpeedMultiplier,
                    }
                instance.movement.speed = game.addVector(shipNewSpeedVector, impactForceVector)
                
                const shieldAngle = game.getAngle(instance.getPosition(), planetPosition)
                instance.activateShield(shieldAngle)
                planet.getHit(1)
            }
        })

        const asteroidsOfGroups = []
        asteroidGroups.forEach(group => {
            if(group.testCollision(instance)) {
                asteroidsOfGroups.push(...group.asteroids)
            }
        })

        asteroidsOfGroups.forEach(asteroid => {
            if(game.distance(instance.getPosition(), asteroid.getPosition()) <= instance.getRadius() + asteroid.getRadius()) {
                const shieldAngle = game.getAngle(instance.getPosition(), asteroid.getPosition())
                instance.activateShield(shieldAngle)
                asteroid.remove()
                app.asteroid.explode(game.systemCont.globalToLocal(asteroid.getPosition().x, asteroid.getPosition().y), "large", asteroid.size)
            }
        })


        // CAMERA ADJUSTMENT
        const viewModeOn = game.precisionMode
        const coords = {
            x: viewModeOn ? app.stage.mouseX : instance.gfx.shipCont.x,
            y: viewModeOn ? app.stage.mouseY : instance.gfx.shipCont.y
        }
        const activeWidth = viewModeOn ? app.canvas_width : game.stageWidth
        const activeHeight = viewModeOn ? app.canvas_height : game.stageHeight
        const mapPadding = viewModeOn ? 50 : game.mapPadding

        let percentX = EasingFunctions.easeInOutSin(
            (coords.x - mapPadding)
            / (activeWidth - mapPadding*2)
        )
        let percentY = EasingFunctions.easeInOutSin(
            (coords.y - mapPadding)
            / (activeHeight - mapPadding*2)
        )

        percentX = (coords.x > activeWidth - mapPadding)
            ? 1 : (coords.x < mapPadding)
            ? 0 : percentX
        percentY = (coords.y > activeHeight - mapPadding)
            ? 1 : (coords.y < mapPadding)
            ? 0 : percentY

        game.systemCont.x = percentX * -(game.stageWidth - app.canvas_width) - 0
        game.systemCont.y = percentY * -(game.stageHeight - app.canvas_height) - 0
    }
            

    const shipMousedown = instance => e => {
        if(instance._deleted || game.status !== game.STATUS_PLAYING)
            return

        if(e.which === 1)
            shoot(instance, e)
        else if(e.which === 3)
            instance.movement.thrust = true
    }

    const shipMouseup = instance => e => {
        if(instance._deleted || game.status !== game.STATUS_PLAYING)
            return

        if(e.which === 3)
            instance.movement.thrust = false
    }

    const shipKeydown = instance => e => {
        if(instance._deleted || game.status !== game.STATUS_PLAYING)
            return

        (e.which === 87) ? instance.movement.thrust = true :
        (e.which === 83) ? instance.movement.back = true :
        (e.which === 65) ? instance.movement.left = true :
        (e.which === 68) ? instance.movement.right = true :
        false
    }

    const shipKeyup = instance => e => {
        if(instance._deleted || game.status !== game.STATUS_PLAYING)
            return

        (e.which === 87) ? instance.movement.thrust = false :
        (e.which === 83) ? instance.movement.back = false :
        (e.which === 65) ? instance.movement.left = false :
        (e.which === 68) ? instance.movement.right = false :
        false
    
    }

    const addControl = (instance) => {
        document.addEventListener("mousedown", shipMousedown(instance))
        document.addEventListener("mouseup", shipMouseup(instance))
        // document.addEventListener("keydown", shipKeydown(instance))
        // document.addEventListener("keyup", shipKeyup(instance))
    }

    let shootLastTime = 0
    const shoot = (instance, e) => {
        if(shootLastTime + app.gameManager.ship.shootDelay*1000 > game.time)
            return

        app.sound.playSFX("blaster")

        shootLastTime = game.time
        const projectileSpeed = 15 * game.scale
        const projectileScale = .11 * game.scale
        const projectileMaxDistance = app.gameManager.ship.projectileMaxDistance * game.scale
        const fadeOutTime = 200 * game.scale
        const gravityShot = false // GRAVITY SHOT

        const sprite = new createjs.Sprite(ship.gfx.blasterSheet)
        sprite.scaleX = sprite.scaleY = projectileScale
        const spritePos = instance.gfx.turretSprite.localToGlobal(0, -40)
        sprite.x = spritePos.x
        sprite.y = spritePos.y
        const originalPosition = spritePos

        const speed = game.polarToCartesian(projectileSpeed, toRad(instance.gfx.turretTransform.rotation + instance.gfx.shipCont.rotation))
        speed.x += instance.movement.speed.x
        speed.y += instance.movement.speed.y
        sprite.rotation = deg(game.getAngle({x: 0, y: 0}, speed, true) + 90)
        sprite.z = 1
        
        game.projectilesCont.addChild(sprite)

        // INSTANCE
        const projectileInstance = {}

        const getPosition = () => 
            game.getWorldPosition(sprite)

        const getRadius = () => 
            110 * projectileScale

        const remove = () => {
            game.projectilesCont.removeChild(sprite)
            projectileInstance._deleted = true
            game.projectiles = game.projectiles.filter(projectile => !projectile._deleted)
        }

        game.addProjectile(Object.assign(projectileInstance, {
            sprite,
            speed,
            getRadius, getPosition,
            originalPosition, projectileMaxDistance, fadeOutTime, _fadeOutStart: false, gravityShot,
            remove, _deleted: false,
        }))
    }

    ship.projectileUpdate = (deltaTime, planets, suns, asteroids, asteroidGroups) => instance => {
        const removeTolerance = 40
        const fadeOutTolerance = .2

        // MOVEMENT
        if(instance.gravityShot) {
            planets.forEach(planet => {
                const planetPos = planet.getPosition()
                const projectilePos = instance.sprite
                const distance = (
                    game.distance(planetPos, projectilePos)
                        - instance.getRadius()
                        - planet.getRadius()
                    )

                if(distance < planet.gravityDistance) {
                    const gravityVectorLength = mapRange(distance, planet.gravityDistance, 0, 0, planet.gravityAcceleration, true, EasingFunctions.easeInQuad) * 3.5
                    const gravityVectorNorm = game.normalize(game.getVector(projectilePos, planetPos))
                    const gravityVector = {x: gravityVectorLength * gravityVectorNorm.x, y: gravityVectorLength * gravityVectorNorm.y}
                    instance.speed.x += gravityVector.x
                    instance.speed.y += gravityVector.y
                }
            })
        }

        instance.sprite.x += instance.speed.x
        instance.sprite.y += instance.speed.y
        instance.sprite.rotation = game.getVectorAngle(instance.speed, true) + 90

        // FADE OUT
        if(game.distance(instance.sprite, instance.originalPosition) > instance.projectileMaxDistance) {
            if(instance._fadeOutStart === false)
                instance._fadeOutStart = game.time
            
            const alpha = mapRange(game.time - instance._fadeOutStart, 0, instance.fadeOutTime, 1, 0, true)
            if(alpha <= .001)
                instance.remove()
            else
                instance.sprite.alpha = alpha
        }

        // ASTEROID HIT
        const asteroidsWithGroups = [...asteroids]
        asteroidGroups.forEach(group => {
            if(group.testCollision(instance)) {
                asteroidsWithGroups.push(...group.asteroids)
            }
        })

        // TODO: REMOVE, DEBUG
        // asteroidsWithGroups.forEach(asteroid => {
        //     const shape = new createjs.Shape()
        //     shape.graphics.f("rgba(255, 50, 50, .5)")
        //         .dc(0, 0, 5)
        //     shape.x = asteroid.getPosition().x
        //     shape.y = asteroid.getPosition().y
        //     game.stage.addChild(shape)
        // })

        asteroidsWithGroups.forEach(asteroid => {
            if(game.distance(instance.sprite, asteroid.getPosition()) <= instance.getRadius() + asteroid.getRadius()) {
                instance.remove()
                if(instance.sprite.alpha > fadeOutTolerance) {
                    asteroid.HP -= 1
                    if(asteroid.HP <= 0) {
                        game.gui.addAsteroidDestroyed()
                        app.sound.playSFX("asteroid_destroy")
                        // const position = !!asteroid.inGroup
                        //     ? game.systemCont.globalToLocal(asteroid.getPosition().x, asteroid.getPosition().y)
                        //     : asteroid.getPosition()
                        
                        const position = game.systemCont.globalToLocal(asteroid.getPosition().x, asteroid.getPosition().y)
                        asteroid.dropLoot()
                        if(asteroid.size === 5) {
                            const newAsteroid = app.asteroid.createAsteroid(
                                "grey",
                                randomInt(2, 3),
                                {
                                    coords: position,
                                    speed: asteroid.speed,
                                    scale: game.scale
                                })
                            game.asteroids.push(newAsteroid)
                            game.systemCont.addChild(newAsteroid.cont)
                            app.asteroid.explode(position, "large", asteroid.size)
                        } else {
                            app.asteroid.explode(position, "small", asteroid.size)
                        }
                        asteroid.remove()
                    } else {
                        const position = game.systemCont.globalToLocal(asteroid.getPosition().x, asteroid.getPosition().y)
                        app.asteroid.explode(position, "large", asteroid.size)
                    }
                }
            }
        })

        const planetsAndSuns = [...planets, ...suns]
        planetsAndSuns.forEach(planet => {
            if(planet.getRadius() + instance.getRadius() >= game.distance(instance.sprite, planet.getPosition())) {
                instance.remove()
                if(instance.sprite.alpha > fadeOutTolerance) {
                    planet.getHit(1)
                }
            }
        })

        // OUT OF BORDERS
        const systemContPosition = game.systemCont.globalToLocal(instance.sprite.x, instance.sprite.y)
        if(systemContPosition.x < 0 - removeTolerance || systemContPosition.x > game.stageWidth + removeTolerance
            || systemContPosition.y < 0 - removeTolerance || systemContPosition.y > game.stageHeight + removeTolerance) {
                instance.remove()
            }
    }

    /*
    * END
    * */
    return ship
})(window.app.ship, window.app.game, window.app, createjs, EasingFunctions)