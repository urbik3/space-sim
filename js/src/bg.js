"use strict"

window.app = window.app || {}
window.app.bg = window.app.bg || {}

/*
 * DOUBLE RANDOM
 * */
window.app.bg = ((bg, game, app, $, createjs, EasingFunctions) => {

    bg.init = () => {
        bg.starfieldCont = new createjs.Container()
        bg.objectsCont = new createjs.Container()
        bg.dustBackCont = new createjs.Container()
        bg.dustFrontCont = new createjs.Container()

        bg.starfieldCont.z = -30
        bg.objectsCont.z = -20
        bg.dustBackCont.z = -10
        bg.dustFrontCont.z = 30

        game.stage.addChild(bg.starfieldCont, bg.objectsCont, bg.dustBackCont, bg.dustFrontCont)
    }

    bg.init1 = bg.init5 = bg.init9 = () => {
        bg.init()

        const starfieldSprite = new createjs.Sprite(
            new createjs.SpriteSheet({
                images: [game.assets[`starfield_small`]],
                frames: {width: 3000, height: 1500, regX: 1500, regY: 750}
        }))

        const starfieldSprite2 = new createjs.Sprite(
            new createjs.SpriteSheet({
                images: [game.assets[`starfield_medium`]],
                frames: {width: 3000, height: 1500, regX: 1500, regY: 750}
        }))
        const starfieldSprite3 = new createjs.Sprite(
            new createjs.SpriteSheet({
                images: [game.assets[`starfield_big`]],
                frames: {width: 3000, height: 1500, regX: 1500, regY: 750}
        }))
        starfieldSprite.x = starfieldSprite2.x = starfieldSprite3.x = game.stageWidth/2
        starfieldSprite.y = starfieldSprite2.y = starfieldSprite3.y = game.stageHeight/2
        bg.starfieldCont.addChild(starfieldSprite, starfieldSprite2, starfieldSprite3)

        const dustSprite = new createjs.Sprite(
            new createjs.SpriteSheet({
                images: [game.assets[`dust_violet`]],
                frames: {width: 1388, height: 774, regX: 694, regY: 387}
        }))
        dustSprite.x = game.stageWidth*3/4
        dustSprite.y = game.stageHeight*3/6
        bg.starfieldCont.addChild(dustSprite)

        bg.starfieldCont.cache(game.stageWidth/2 - 1500, game.stageHeight/2 - 750, game.stageWidth/2 + 1500, game.stageHeight/2 + 750)
    }

    bg.init2 = bg.init6 = bg.init10 = () => {
        bg.init()

        const starfieldSprite = new createjs.Sprite(
            new createjs.SpriteSheet({
                images: [game.assets[`starfield_small`]],
                frames: {width: 3000, height: 1500, regX: 1500, regY: 750}
        }))
        const starfieldSprite2 = new createjs.Sprite(
            new createjs.SpriteSheet({
                images: [game.assets[`starfield_medium`]],
                frames: {width: 3000, height: 1500, regX: 1500, regY: 750}
        }))
        const starfieldSprite3 = new createjs.Sprite(
            new createjs.SpriteSheet({
                images: [game.assets[`starfield_big`]],
                frames: {width: 3000, height: 1500, regX: 1500, regY: 750}
        }))
        starfieldSprite.x = starfieldSprite2.x = starfieldSprite3.x = game.stageWidth/2
        starfieldSprite.y = starfieldSprite2.y = starfieldSprite3.y = game.stageHeight/2
        bg.starfieldCont.addChild(starfieldSprite, starfieldSprite2, starfieldSprite3)

        const dustSprite = new createjs.Sprite(
            new createjs.SpriteSheet({
                images: [game.assets[`dust_blue`]],
                frames: {width: 1184, height: 842, regX: 592, regY: 421}
        }))
        dustSprite.x = game.stageWidth*3/4
        dustSprite.y = game.stageHeight*3/6
        bg.starfieldCont.addChild(dustSprite)

        bg.starfieldCont.cache(game.stageWidth/2 - 1500, game.stageHeight/2 - 750, game.stageWidth/2 + 1500, game.stageHeight/2 + 750)

        const nebulaeSprite = new createjs.Sprite(
            new createjs.SpriteSheet({
                images: [game.assets[`nebulae_cold`]],
                frames: {width: 1250, height: 1345, regX: 965, regY: 1040}
        }))
        nebulaeSprite.x = game.stageWidth*2/4
        nebulaeSprite.y = game.stageHeight*2/4
        bg.dustFrontCont.addChild(nebulaeSprite)

        let planetSprite = new createjs.Sprite(new createjs.SpriteSheet({
            images: [game.assets[`planet6`]], 
            frames: {width: 150, height: 150, regX: 75, regY: 75}
        }))
        planetSprite.width = planetSprite.height = 150
        planetSprite.x = game.stageWidth * 3/4
        planetSprite.y = game.stageHeight * 2/4
        bg.dustFrontCont.addChild(planetSprite)

        let planetSprite2 = new createjs.Sprite(new createjs.SpriteSheet({
            images: [game.assets[`planet2`]], 
            frames: {width: 150, height: 150, regX: 75, regY: 75}
        }))
        planetSprite2.width = planetSprite2.height = 150
        planetSprite2.x = game.stageWidth * 1/2
        planetSprite2.y = game.stageHeight * 1/4
        planetSprite2.scaleX = planetSprite2.scaleY = .7
        planetSprite2.filters = [new createjs.ColorFilter(.5, .5, .5, 1, 0,0,0,0)]
        planetSprite2.cache(-75, -75, 150, 150)
        bg.objectsCont.addChild(planetSprite2)
    }

    bg.init3 = bg.init7 = bg.init11 = () => {
        bg.init()

        const starfieldSprite = new createjs.Sprite(
            new createjs.SpriteSheet({
                images: [game.assets[`starfield_small`]],
                frames: {width: 3000, height: 1500, regX: 1500, regY: 750}
        }))

        const starfieldSprite2 = new createjs.Sprite(
            new createjs.SpriteSheet({
                images: [game.assets[`starfield_medium`]],
                frames: {width: 3000, height: 1500, regX: 1500, regY: 750}
        }))
        const starfieldSprite3 = new createjs.Sprite(
            new createjs.SpriteSheet({
                images: [game.assets[`starfield_big`]],
                frames: {width: 3000, height: 1500, regX: 1500, regY: 750}
        }))
        starfieldSprite.x = starfieldSprite2.x = starfieldSprite3.x = game.stageWidth/2
        starfieldSprite.y = starfieldSprite2.y = starfieldSprite3.y = game.stageHeight/2
        bg.starfieldCont.addChild(starfieldSprite, starfieldSprite2, starfieldSprite3)

        const dustSprite = new createjs.Sprite(
            new createjs.SpriteSheet({
                images: [game.assets[`dust_violet`]],
                frames: {width: 1388, height: 774, regX: 694, regY: 387}
        }))
        dustSprite.x = game.stageWidth*3/4
        dustSprite.y = game.stageHeight*3/6
        bg.starfieldCont.addChild(dustSprite)

        bg.starfieldCont.cache(game.stageWidth/2 - 1500, game.stageHeight/2 - 750, game.stageWidth/2 + 1500, game.stageHeight/2 + 750)
    }

    bg.init4 = bg.init8 = bg.init12 = () => {
        bg.init()

        const starfieldSprite = new createjs.Sprite(
            new createjs.SpriteSheet({
                images: [game.assets[`starfield_small`]],
                frames: {width: 3000, height: 1500, regX: 1500, regY: 750}
        }))

        const starfieldSprite2 = new createjs.Sprite(
            new createjs.SpriteSheet({
                images: [game.assets[`starfield_medium`]],
                frames: {width: 3000, height: 1500, regX: 1500, regY: 750}
        }))
        const starfieldSprite3 = new createjs.Sprite(
            new createjs.SpriteSheet({
                images: [game.assets[`starfield_big`]],
                frames: {width: 3000, height: 1500, regX: 1500, regY: 750}
        }))
        starfieldSprite.x = starfieldSprite2.x = starfieldSprite3.x = game.stageWidth/2
        starfieldSprite.y = starfieldSprite2.y = starfieldSprite3.y = game.stageHeight/2
        bg.starfieldCont.addChild(starfieldSprite, starfieldSprite2, starfieldSprite3)

        const dustSprite = new createjs.Sprite(
            new createjs.SpriteSheet({
                images: [game.assets[`dust_yellow`]],
                frames: {width: 1316, height: 683, regX: 694, regY: 387}
        }))
        dustSprite.x = game.stageWidth*3/4
        dustSprite.y = game.stageHeight*3/6
        bg.starfieldCont.addChild(dustSprite)

        bg.starfieldCont.cache(game.stageWidth/2 - 1500, game.stageHeight/2 - 750, game.stageWidth/2 + 1500, game.stageHeight/2 + 750)

        const nebulaeSprite = new createjs.Sprite(
            new createjs.SpriteSheet({
                images: [game.assets[`nebulae_hot`]],
                frames: {width: 1071, height: 1328, regX: 0, regY: 0}
        }))
        nebulaeSprite.x = -100
        nebulaeSprite.y = game.stageHeight*0
        bg.dustFrontCont.addChild(nebulaeSprite)
    }

    bg.update = (deltaTime, ship) => {
        const starfieldParallaxSpeed = .6
        const objectsParallaxSpeed = .8
        const dustBackParallaxSpeed = 1
        const dustFrontParallaxSpeed = 1
        
        const viewModeOn = game.precisionMode
        const coords = {
            x: viewModeOn ? app.stage.mouseX : ship.gfx.shipCont.x,
            y: viewModeOn ? app.stage.mouseY : ship.gfx.shipCont.y
        }
        const activeWidth = viewModeOn ? app.canvas_width : game.stageWidth
        const activeHeight = viewModeOn ? app.canvas_height : game.stageHeight
        const mapPadding = viewModeOn ? 50 : game.mapPadding

        let percentX = EasingFunctions.easeInOutSin(
            (coords.x - mapPadding)
            / (activeWidth - mapPadding*2)
        )
        let percentY = EasingFunctions.easeInOutSin(
            (coords.y - mapPadding)
            / (activeHeight - mapPadding*2)
        )

        percentX = (coords.x > activeWidth - mapPadding)
            ? 1 : (coords.x < mapPadding)
            ? 0 : percentX
        percentY = (coords.y > activeHeight - mapPadding)
            ? 1 : (coords.y < mapPadding)
            ? 0 : percentY

        const parallaxValueX = percentX * -(game.stageWidth - app.canvas_width) - 0
        const parallaxValueY = percentY * -(game.stageHeight - app.canvas_height) - 0

        bg.starfieldCont.x = parallaxValueX * starfieldParallaxSpeed
        bg.starfieldCont.y = parallaxValueY * starfieldParallaxSpeed
        bg.objectsCont.x = parallaxValueX * objectsParallaxSpeed
        bg.objectsCont.y = parallaxValueY * objectsParallaxSpeed
        bg.dustBackCont.x = parallaxValueX * dustBackParallaxSpeed
        bg.dustBackCont.y = parallaxValueY * dustBackParallaxSpeed
        bg.dustFrontCont.x = parallaxValueX * dustFrontParallaxSpeed
        bg.dustFrontCont.y = parallaxValueY * dustFrontParallaxSpeed
    }

    return bg

})(window.app.bg, window.app.game, window.app, jQuery, createjs, EasingFunctions)