"use strict"

window.app = window.app || {}
window.app.game = window.app.game || {}

window.app = (function(app, $, createjs) {

    app.stage = false
    app.renderer = false
    app.ticker = {}

    app.fps = 60
    app.mspf = 1000/app.fps

    app.changeFPS = fps => {
        app.fps = fps
        app.mspf = 1000/app.fps
    }

    let canvas
    app.canvas_width
    app.canvas_height

    app.initCallbacks = []
    app.addInitCallback = function(fn) {
        app.initCallbacks.push(fn)
    }

    /*
     * INIT & UTILS
     * */

    app.init = () => {
        // IniT STAGE
        canvas = document.createElement("canvas")
        document.body.appendChild(canvas)
        canvas.id = "game"
        console.log(canvas)
        app.stage = new createjs.Stage(canvas)
        app.stage.enableMouseOver(100)
        // app.stage.setClearColor("#000")

        // INIT RESIZE
        let $window = $(window)
        $window.resize(app.resize)
        app.resize({}, true)

        // INIT APP
        document.addEventListener('contextmenu', e=>e.preventDefault(), false)
        app.screenManager.showLoading()
        app.game.preload()
            .then(() => {
                try {
                    app.initCallbacks.forEach(fn=>fn(app))
                    app.play()
                } catch(e) {
                    console.log(e)
                }
            })
    }

    app.stop = () => {
        createjs.Ticker.reset()
    }

    app.play = () => {
        console.log("play")
        createjs.Ticker.init()
        createjs.Ticker.framerate = app.fps
        app.screenManager.init()
        // createjs.Ticker.timingMode = createjs.Ticker.RAF_SYNCHED
        createjs.Ticker.addEventListener("tick", app.update)
    }

    app.update = (tick) => {
        // console.log("update", deltaTime)
        app.screenManager.update(tick.delta)
        app.stage.update()
        // app.stop()
    }

    app.resize = (e, firstResize = false) => {
        app.canvas_width = 1024//window.innerWidth
        app.canvas_height = 768//window.innerHeight
        canvas.style.height = app.canvas_height+"px"
        canvas.style.width = app.canvas_width+"px"
        app.stage.canvas.height = app.canvas_height
        app.stage.canvas.width = app.canvas_width
        if(firstResize !== true)
            app.game.resize()
    }

    return app
})(window.app, jQuery, createjs)