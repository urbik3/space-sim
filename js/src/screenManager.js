"use strict"

window.app = window.app || {}
window.app.screenManager = window.app.screenManager || {}
window.app.game = window.app.game || {}

/*
 * DOUBLE RANDOM
 * */
window.app.screenManager = ((screenManager, app, game, $, createjs, EasingFunctions) => {

    screenManager.STATUS_LOADING = "loading"
    screenManager.STATUS_MENU = "menu"
    screenManager.STATUS_SHOP = "shop"
    screenManager.STATUS_GAME = "game"
    screenManager.STATUS_PAUSE = "pause"
    screenManager.status = screenManager.STATUS_LOADING

    screenManager.loadingCont
    screenManager.showLoading = () => {
        screenManager.loadingCont = new createjs.Container()
        screenManager.loadingCont.x = app.canvas_width/2
        screenManager.loadingCont.y = app.canvas_height/2

        const loader = new createjs.Container()
        loader.y = 20
        screenManager.loadingCont.addChild(loader)

        const timeline = new TimelineMax({repeat: -1, yoyo: false, onUpdate: function() {
            app.stage.update()
        }})
        const loadersNum = 3
        const loadersSpacing = 30
        generateArr(0, loadersNum-1)
            .map(i => {
                const loadingCircle = new createjs.Shape()
                const loadingCircleCommand = loadingCircle.graphics
                    .f("#007acc")
                    .dc(0, 0, 5)
                loadingCircle.x = i * loadersSpacing
                loader.addChild(loadingCircle)

                app.stage.addChild(screenManager.loadingCont)
                app.stage.update()
        
                timeline.to([loadingCircleCommand.command], .5, {radius: 10}, "-=.5")
                timeline.to([loadingCircleCommand.command], .5, {radius: 5})
            })
        loader.x = -(loadersNum/2*loadersSpacing)
        
        const txt = new createjs.Text()
        txt.font = "bold 32px Coda"
        txt.color = "#007acc"
        txt.text = "Loading"
        txt.textAlign = "center"
        txt.x = -10
        txt.y = -txt.getBounds().height
        screenManager.loadingCont.addChild(txt)
    }

    const hideLoading = () => {
        app.stage.removeChild(screenManager.loadingCont)
    }
    app.addInitCallback(hideLoading)

    screenManager.playGame = (levelNumber = 1) => {
        app.stage.removeChild(screenManager.menuCont)
        game.init(levelNumber)
        game.sortGfx(app.stage)
        screenManager.status = screenManager.STATUS_GAME
    }

    screenManager.pauseMenuCont
    screenManager.showPauseMenu = () => {
        screenManager.pauseMenuCont = new createjs.Container()
        screenManager.pauseMenuCont.z = 10
        const bg = new createjs.Shape()
        bg.graphics
            .f("rgba(0, 0, 0, .75)")
            .dr(0, 0, app.canvas_width, app.canvas_height)
        screenManager.pauseMenuCont.addChild(bg)

        const buttonsCont = new createjs.Container()
        buttonsCont.x = app.canvas_width / 2
        buttonsCont.y = app.canvas_height / 3

        const width = 160
        buttonsCont.addChild(screenManager.btn("Main Menu", width, {x: -width/2, y: 0}, () => {
            screenManager.hidePauseMenu()
            game.clearStage()
            app.stage.removeChild(game.stage)
            showMenu()
        }, {}))

        buttonsCont.addChild(screenManager.btn("Close", width, {x: -width/2, y: 80}, () => {
                game.status = game.STATUS_PLAYING
                app.screenManager.hidePauseMenu()
        }, {}))

        screenManager.pauseMenuCont.addChild(buttonsCont)
        app.stage.addChild(screenManager.pauseMenuCont)
        screenManager.status = screenManager.STATUS_PAUSE
        game.sortGfx(app.stage)
    }

    screenManager.hidePauseMenu = () => {
        app.stage.removeChild(screenManager.pauseMenuCont)
        screenManager.status = screenManager.STATUS_GAME
    }

    screenManager.endgameCont
    screenManager.showEndgame = (lost = true, reason = "") => {
        screenManager.endgameCont = new createjs.Container()
        screenManager.endgameCont.z = 10

        const bg = new createjs.Shape()
        bg.graphics
            .f("rgba(0, 0, 0, .75)")
            .dr(0, 0, app.canvas_width, app.canvas_height)
        screenManager.endgameCont.addChild(bg)

        const buttonsCont = new createjs.Container()
        buttonsCont.x = app.canvas_width / 2
        buttonsCont.y = app.canvas_height / 2

        const width = 160
        buttonsCont.addChild(screenManager.btn("Continue", width, {x: -width/2, y: 0}, () => {
            app.stage.removeChild(screenManager.endgameCont)
            game.clearStage()
            app.stage.removeChild(game.stage)
            showMenu()
        }, {}))

        // HEADLINE
        const headline = new createjs.Text()
        headline.font = "bold 48px Coda"
        headline.color = "#007acc"
        headline.text = lost ? "You lost" : "You win"
        headline.x = 0
        headline.y = -150
        headline.z = 1
        headline.textAlign = "center"
        buttonsCont.addChild(headline)

        const reasonText = new createjs.Text()
        reasonText.font = "bold 16px Coda"
        reasonText.color = "#007acc"
        reasonText.text = reason
        reasonText.x = 0
        reasonText.y = -100
        reasonText.z = 1
        reasonText.textAlign = "center"
        buttonsCont.addChild(reasonText)

        const score = new createjs.Text();
        score.font = "bold 24px Coda";
        score.color = "#007acc";
        score.text = (app.game.gui.getScore() > app.gameManager.game.levels[game.levelNumber-1].score && !lost
                    ? "New high score: " : "Score: ")
                    + printNumber(app.game.gui.getScore()) + " / " + printNumber(app.gameManager.game.levels[game.levelNumber-1].score)
        score.x = 0
        score.y = -40
        score.z = 1
        score.textAlign = "center"
        buttonsCont.addChild(score)

        screenManager.endgameCont.addChild(buttonsCont)
        app.stage.addChild(screenManager.endgameCont)
        screenManager.status = screenManager.STATUS_ENDGAME
        game.sortGfx(app.stage)
    }

    screenManager.menuCont
    screenManager.menuBgCont
    const showMenu = () => {
        screenManager.status = screenManager.STATUS_MENU
        app.sound.playMusic("main_music")

        // MENU CONT
        app.stage.removeChild(screenManager.menuCont)
        screenManager.menuCont = new createjs.Container()
        screenManager.menuCont.z = 1

        // OPTIONS
        const btnPadding = 20
        const levelPadding = 30

        // SUN
        const spriteSheet = new createjs.SpriteSheet({
            images: [game.assets[`sun`]], 
            frames: {width: 330, height: 330, regX: 165, regY: 165, count: 15},
            animations: {
                shine: [0, 14, "shine", .1]
            },
            framerate: .5
        })
        const sunSprite = new createjs.Sprite(spriteSheet, "shine")
        sunSprite.scaleX = sunSprite.scaleY = .5
        sunSprite.x = 130
        sunSprite.y = 90
        sunSprite.z = 1
        screenManager.menuCont.addChild(sunSprite)

        let planetSprite1 = new createjs.Sprite(new createjs.SpriteSheet({
            images: [game.assets[`planet2`]], 
            frames: {width: 150, height: 150, regX: 75, regY: 75}
        }))
        planetSprite1.scaleX = planetSprite1.scaleY = .2
        planetSprite1.x = 50
        planetSprite1.y = 50
        planetSprite1.z = 0.5
        screenManager.menuCont.addChild(planetSprite1)

        // HEADLINE
        const headline = new createjs.Text();
        headline.font = "bold 72px Coda";
        headline.color = "#007acc";
        headline.text = "Solar Defense";
        headline.x = 240
        headline.y = 63
        headline.z = 1
        screenManager.menuCont.addChild(headline)

        initLevels()
        initShop()

        // BG
        const bgCont = screenManager.menuBgCont = new createjs.Container()

        const starfieldSprite = new createjs.Sprite(
            new createjs.SpriteSheet({
                images: [game.assets[`starfield_small`]],
                frames: {width: 3000, height: 1500, regX: 1500, regY: 750}
        }))
        const starfieldSprite2 = new createjs.Sprite(
            new createjs.SpriteSheet({
                images: [game.assets[`starfield_medium`]],
                frames: {width: 3000, height: 1500, regX: 1500, regY: 750}
        }))
        const starfieldSprite3 = new createjs.Sprite(
            new createjs.SpriteSheet({
                images: [game.assets[`starfield_big`]],
                frames: {width: 3000, height: 1500, regX: 1500, regY: 750}
        }))
        starfieldSprite.x = starfieldSprite2.x = starfieldSprite3.x = game.stageWidth/2
        starfieldSprite.y = starfieldSprite2.y = starfieldSprite3.y = game.stageHeight/2
        bgCont.addChild(starfieldSprite, starfieldSprite2, starfieldSprite3)

        const nebulaeSprite = new createjs.Sprite(
            new createjs.SpriteSheet({
                images: [game.assets[`nebulae_cold`]],
                frames: {width: 1250, height: 1345, regX: 965, regY: 1040}
        }))
        nebulaeSprite.x = 500
        nebulaeSprite.y = 400
        nebulaeSprite.rotation = 30
        bgCont.addChild(nebulaeSprite)

        bgCont.z = 0
        screenManager.menuCont.addChild(bgCont)
        game.sortGfx(screenManager.menuCont)
        app.stage.addChild(screenManager.menuCont)
    }

    const initLevels = () => {
        if(screenManager.levelCont)
            screenManager.menuCont.removeChild(levelCont)

        const levelCont = screenManager.levelCont = new createjs.Container()
        levelCont.x = 30
        levelCont.x = (screenManager.status === screenManager.STATUS_MENU) ? 30 : -app.canvas_width
        levelCont.y = -50
        levelCont.z = 1

        const width = 200
        levelCont.addChild(screenManager.btn(
            "Oort cloud", width, {x: 0, y: app.canvas_height - 410},
            () => screenManager.playGame(1),
            {
                btnPadding: 35,
                isHidden: !app.gameManager.game.levels[0].available,
                hasBg: !app.gameManager.game.levels[0].won,
                topText: "Level 1",
                bottomText: printNumber(app.gameManager.game.levels[0].score)
            }))
        levelCont.addChild(screenManager.btn(
            "Rogue planets", width, {x: 0, y: app.canvas_height - 300},
            () => screenManager.playGame(2),
            {
                btnPadding: 35,
                isHidden: !app.gameManager.game.levels[1].available,
                hasBg: !app.gameManager.game.levels[1].won,
                topText: "Level 1",
                bottomText: printNumber(app.gameManager.game.levels[1].score)
            }))
        levelCont.addChild(screenManager.btn(
            "Solar defense", width, {x: 0, y: app.canvas_height - 190},
            () => screenManager.playGame(3),
            {
                btnPadding: 35,
                isHidden: !app.gameManager.game.levels[2].available,
                hasBg: !app.gameManager.game.levels[2].won,
                topText: "Level 1",
                bottomText: printNumber(app.gameManager.game.levels[2].score)
            }))
        levelCont.addChild(screenManager.btn(
            "Black holes", width, {x: 0, y: app.canvas_height - 80},
            () => screenManager.playGame(4),
            {
                btnPadding: 35,
                isHidden: !app.gameManager.game.levels[3].available,
                hasBg: !app.gameManager.game.levels[3].won,
                topText: "Level 1",
                bottomText: printNumber(app.gameManager.game.levels[3].score)
            }))
        // NEXT COLUMN
        levelCont.addChild(screenManager.btn(
            "Oort cloud", width, {x: width + 20, y: app.canvas_height - 410},
            () => screenManager.playGame(5),
            {
                btnPadding: 35,
                isHidden: !app.gameManager.game.levels[4].available,
                hasBg: !app.gameManager.game.levels[4].won,
                topText: "Level 2",
                bottomText: printNumber(app.gameManager.game.levels[4].score)
            }))
        levelCont.addChild(screenManager.btn(
            "Rogue planets", width, {x: width + 20, y: app.canvas_height - 300},
            () => screenManager.playGame(6),
            {
                btnPadding: 35,
                isHidden: !app.gameManager.game.levels[5].available,
                hasBg: !app.gameManager.game.levels[5].won,
                topText: "Level 2",
                bottomText: printNumber(app.gameManager.game.levels[5].score)
            }))
        levelCont.addChild(screenManager.btn(
            "Solar defense", width, {x: width + 20, y: app.canvas_height - 190},
            () => screenManager.playGame(7),
            {
                btnPadding: 35,
                isHidden: !app.gameManager.game.levels[6].available,
                hasBg: !app.gameManager.game.levels[6].won,
                topText: "Level 2",
                bottomText: printNumber(app.gameManager.game.levels[6].score)
            }))
        levelCont.addChild(screenManager.btn(
            "Black holes", width, {x: width + 20, y: app.canvas_height - 80},
            () => screenManager.playGame(8),
            {
                btnPadding: 35,
                isHidden: !app.gameManager.game.levels[7].available,
                hasBg: !app.gameManager.game.levels[7].won,
                topText: "Level 2",
                bottomText: printNumber(app.gameManager.game.levels[7].score)
            }))
        // NEXT COLUMN
        levelCont.addChild(screenManager.btn(
            "Oort cloud", width, {x: (width + 20) * 2, y: app.canvas_height - 410},
            () => screenManager.playGame(9),
            {
                btnPadding: 35,
                isHidden: !app.gameManager.game.levels[8].available,
                hasBg: !app.gameManager.game.levels[8].won,
                topText: "Level 3",
                bottomText: printNumber(app.gameManager.game.levels[8].score)
            }))
        levelCont.addChild(screenManager.btn(
            "Rogue planets", width, {x: (width + 20) * 2, y: app.canvas_height - 300},
            () => screenManager.playGame(10),
            {
                btnPadding: 35,
                isHidden: !app.gameManager.game.levels[9].available,
                hasBg: !app.gameManager.game.levels[9].won,
                topText: "Level 3",
                bottomText: printNumber(app.gameManager.game.levels[9].score)
            }))
        levelCont.addChild(screenManager.btn(
            "Solar defense", width, {x: (width + 20) * 2, y: app.canvas_height - 190},
            () => screenManager.playGame(11),
            {
                btnPadding: 35,
                isHidden: !app.gameManager.game.levels[10].available,
                hasBg: !app.gameManager.game.levels[10].won,
                topText: "Level 3",
                bottomText: printNumber(app.gameManager.game.levels[10].score)
            }))
        levelCont.addChild(screenManager.btn(
            "Black holes", width, {x: (width + 20) * 2, y: app.canvas_height - 80},
            () => screenManager.playGame(12),
            {
                btnPadding: 35,
                isHidden: !app.gameManager.game.levels[11].available,
                hasBg: !app.gameManager.game.levels[11].won,
                topText: "Level 3",
                bottomText: printNumber(app.gameManager.game.levels[11].score)
            }))
        
        // SCORE & SHOP BTN
        levelCont.addChild(screenManager.btn(
            printNumber(app.gameManager.getAvailableScore()), width, {x: app.canvas_width - width - 60, y: app.canvas_height - 190},
            () => {},
            {
                btnPadding: 35,
                stagger: false,
                topText: "Available score",
                bottomText: printNumber(app.gameManager.getScore()),
                hasFunction: false,
            }))
        
        levelCont.addChild(screenManager.btn(
            "Upgrades", width, {x: app.canvas_width - width - 60, y: app.canvas_height - 80},
            () => {
                initShop()
                screenManager.slideInShop()
            },
            {
                btnPadding: 35,
                stagger: false,
            }))

        screenManager.menuCont.addChild(levelCont)
    }

    const initShop = () => {
        if(screenManager.shopCont)
            screenManager.menuCont.removeChild(screenManager.shopCont)

        screenManager.shopCont = new createjs.Container()
        screenManager.shopCont.x = (screenManager.status === screenManager.STATUS_SHOP) ? 30 : app.canvas_width + 30
        screenManager.shopCont.y = -50
        screenManager.shopCont.z = 1

        const width = 200,
            padding = 20
        
        // SPEED
        screenManager.shopCont.addChild(screenManager.btn(
            "Sell", width, {x: 0, y: app.canvas_height - 520},
            () => {
                app.gameManager.ship.speedMax = app.gameManager.upgrades.speedMax[0]
                app.gameManager.ship.speedAcc = app.gameManager.upgrades.speedAcc[0]
                app.gameManager.save(app.gameManager.ITEM_SHIP)

                app.gameManager.upgradeUI.jetSpeed.forEach(btn => btn.isSelected = false)
                app.gameManager.upgradeUI.jetSpeed[0].isSelected = true
                app.gameManager.save(app.gameManager.ITEM_UPGRADE_UI)
                initShop()
            },
            {
                btnPadding: 35,
                hasBg: false,
                stagger: false,
            }))
        screenManager.shopCont.addChild(screenManager.btn(
            "Jet speed", width, {x: width + padding, y: app.canvas_height - 520},
            () => {
                app.gameManager.ship.speedMax = app.gameManager.upgrades.speedMax[1]
                app.gameManager.ship.speedAcc = app.gameManager.upgrades.speedAcc[1]
                app.gameManager.save(app.gameManager.ITEM_SHIP)
                app.gameManager.upgradeUI.jetSpeed.forEach(btn => btn.isSelected = false)
                app.gameManager.upgradeUI.jetSpeed[1].isSelected = true
                app.gameManager.save(app.gameManager.ITEM_UPGRADE_UI)
                initShop()
            },
            {
                btnPadding: 35,
                hasBg: app.gameManager.upgradeUI.jetSpeed[1].isSelected,
                isDisabled: (app.gameManager.upgradeUI.jetSpeed[1].costScore > app.gameManager.getAvailableScore() && !app.gameManager.upgradeUI.jetSpeed[1].isSelected),
                topText: "400",
                bottomText: printNumber(app.gameManager.upgradeUI.jetSpeed[1].costScore),
                stagger: false,
            }))
        screenManager.shopCont.addChild(screenManager.btn(
            "Jet speed", width, {x: (width + padding) * 2, y: app.canvas_height - 520},
            () => {
                app.gameManager.ship.speedMax = app.gameManager.upgrades.speedMax[2]
                app.gameManager.ship.speedAcc = app.gameManager.upgrades.speedAcc[2]
                app.gameManager.save(app.gameManager.ITEM_SHIP)
                app.gameManager.upgradeUI.jetSpeed.forEach(btn => btn.isSelected = false)
                app.gameManager.upgradeUI.jetSpeed[2].isSelected = true
                app.gameManager.save(app.gameManager.ITEM_UPGRADE_UI)
                initShop()
            },
            {
                btnPadding: 35,
                hasBg: app.gameManager.upgradeUI.jetSpeed[2].isSelected,
                isDisabled: (app.gameManager.upgradeUI.jetSpeed[2].costScore > app.gameManager.getAvailableScore() && !app.gameManager.upgradeUI.jetSpeed[2].isSelected),
                topText: "700",
                bottomText: printNumber(app.gameManager.upgradeUI.jetSpeed[2].costScore),
                stagger: false,
            }))
        
        // ROTATION
        screenManager.shopCont.addChild(screenManager.btn(
            "Sell", width, {x: 0, y: app.canvas_height - 410},
            () => {
                app.gameManager.ship.shipRotationAcc = app.gameManager.upgrades.shipRotationAcc[0]
                app.gameManager.ship.shipRotationSpeed = app.gameManager.upgrades.shipRotationSpeed[0]
                app.gameManager.save(app.gameManager.ITEM_SHIP)
                app.gameManager.upgradeUI.rotationSpeed.forEach(btn => btn.isSelected = false)
                app.gameManager.upgradeUI.rotationSpeed[0].isSelected = true
                app.gameManager.save(app.gameManager.ITEM_UPGRADE_UI)
                initShop()
            },
            {
                btnPadding: 35,
                hasBg: false,
                stagger: false,
            }))
        screenManager.shopCont.addChild(screenManager.btn(
            "Rotation speed", width, {x: width + padding, y: app.canvas_height - 410},
            () => {
                app.gameManager.ship.shipRotationAcc = app.gameManager.upgrades.shipRotationAcc[1]
                app.gameManager.ship.shipRotationSpeed = app.gameManager.upgrades.shipRotationSpeed[1]
                app.gameManager.save(app.gameManager.ITEM_SHIP)
                app.gameManager.upgradeUI.rotationSpeed.forEach(btn => btn.isSelected = false)
                app.gameManager.upgradeUI.rotationSpeed[1].isSelected = true
                app.gameManager.save(app.gameManager.ITEM_UPGRADE_UI)
                initShop()
            },
            {
                btnPadding: 35,
                hasBg: app.gameManager.upgradeUI.rotationSpeed[1].isSelected,
                isDisabled: (app.gameManager.upgradeUI.rotationSpeed[1].costScore > app.gameManager.getAvailableScore() && !app.gameManager.upgradeUI.rotationSpeed[1].isSelected),
                topText: "200",
                bottomText: printNumber(app.gameManager.upgradeUI.rotationSpeed[1].costScore),
                stagger: false,
            }))
        screenManager.shopCont.addChild(screenManager.btn(
            "Rotation speed", width, {x: (width + padding) * 2, y: app.canvas_height - 410},
            () => {
                app.gameManager.ship.shipRotationAcc = app.gameManager.upgrades.shipRotationAcc[2]
                app.gameManager.ship.shipRotationSpeed = app.gameManager.upgrades.shipRotationSpeed[2]
                app.gameManager.save(app.gameManager.ITEM_SHIP)
                app.gameManager.upgradeUI.rotationSpeed.forEach(btn => btn.isSelected = false)
                app.gameManager.upgradeUI.rotationSpeed[2].isSelected = true
                app.gameManager.save(app.gameManager.ITEM_UPGRADE_UI)
                initShop()
            },
            {
                btnPadding: 35,
                hasBg: app.gameManager.upgradeUI.rotationSpeed[2].isSelected,
                isDisabled: (app.gameManager.upgradeUI.rotationSpeed[2].costScore > app.gameManager.getAvailableScore() && !app.gameManager.upgradeUI.rotationSpeed[2].isSelected),
                topText: "500",
                bottomText: printNumber(app.gameManager.upgradeUI.rotationSpeed[2].costScore),
                stagger: false,
            }))

        // TURRET ROTATION
        screenManager.shopCont.addChild(screenManager.btn(
            "Sell", width, {x: 0, y: app.canvas_height - 300},
            () => {
                app.gameManager.ship.turretRotation = app.gameManager.upgrades.turretRotation[0]
                app.gameManager.save(app.gameManager.ITEM_SHIP)
                app.gameManager.upgradeUI.turretRotation.forEach(btn => btn.isSelected = false)
                app.gameManager.upgradeUI.turretRotation[0].isSelected = true
                app.gameManager.save(app.gameManager.ITEM_UPGRADE_UI)
                initShop()
            },
            {
                btnPadding: 35,
                hasBg: false,
                stagger: false,
            }))
        screenManager.shopCont.addChild(screenManager.btn(
            "Turret rotation", width, {x: width + padding, y: app.canvas_height - 300},
            () => {
                app.gameManager.ship.turretRotation = app.gameManager.upgrades.turretRotation[1]
                app.gameManager.save(app.gameManager.ITEM_SHIP)
                app.gameManager.upgradeUI.turretRotation.forEach(btn => btn.isSelected = false)
                app.gameManager.upgradeUI.turretRotation[1].isSelected = true
                app.gameManager.save(app.gameManager.ITEM_UPGRADE_UI)
                initShop()
            },
            {
                btnPadding: 35,
                hasBg: app.gameManager.upgradeUI.turretRotation[1].isSelected,
                isDisabled: (app.gameManager.upgradeUI.turretRotation[1].costScore > app.gameManager.getAvailableScore() && !app.gameManager.upgradeUI.turretRotation[1].isSelected),
                topText: "20",
                bottomText: printNumber(app.gameManager.upgradeUI.turretRotation[1].costScore),
                stagger: false,
            }))
        screenManager.shopCont.addChild(screenManager.btn(
            "Turret rotation", width, {x: (width + padding) * 2, y: app.canvas_height - 300},
            () => {
                app.gameManager.ship.turretRotation = app.gameManager.upgrades.turretRotation[2]
                app.gameManager.save(app.gameManager.ITEM_SHIP)
                app.gameManager.upgradeUI.turretRotation.forEach(btn => btn.isSelected = false)
                app.gameManager.upgradeUI.turretRotation[2].isSelected = true
                app.gameManager.save(app.gameManager.ITEM_UPGRADE_UI)
                initShop()
            },
            {
                btnPadding: 35,
                hasBg: app.gameManager.upgradeUI.turretRotation[2].isSelected,
                isDisabled: (app.gameManager.upgradeUI.turretRotation[2].costScore > app.gameManager.getAvailableScore() && !app.gameManager.upgradeUI.turretRotation[2].isSelected),
                topText: "60",
                bottomText: printNumber(app.gameManager.upgradeUI.turretRotation[2].costScore),
                stagger: false,
            }))

        // TURRET QUALITY
        screenManager.shopCont.addChild(screenManager.btn(
            "Sell", width, {x: 0, y: app.canvas_height - 190},
            () => {
                app.gameManager.ship.projectileMaxDistance = app.gameManager.upgrades.projectileMaxDistance[0]
                app.gameManager.ship.shootDelay = app.gameManager.upgrades.shootDelay[0]
                app.gameManager.save(app.gameManager.ITEM_SHIP)
                app.gameManager.upgradeUI.turretQuality.forEach(btn => btn.isSelected = false)
                app.gameManager.upgradeUI.turretQuality[0].isSelected = true
                app.gameManager.save(app.gameManager.ITEM_UPGRADE_UI)
                initShop()
            },
            {
                btnPadding: 35,
                hasBg: false,
                stagger: false,
            }))
        screenManager.shopCont.addChild(screenManager.btn(
            "Turret quality", width, {x: width + padding, y: app.canvas_height - 190},
            () => {
                app.gameManager.ship.projectileMaxDistance = app.gameManager.upgrades.projectileMaxDistance[1]
                app.gameManager.ship.shootDelay = app.gameManager.upgrades.shootDelay[1]
                app.gameManager.save(app.gameManager.ITEM_SHIP)
                app.gameManager.upgradeUI.turretQuality.forEach(btn => btn.isSelected = false)
                app.gameManager.upgradeUI.turretQuality[1].isSelected = true
                app.gameManager.save(app.gameManager.ITEM_UPGRADE_UI)
                initShop()
            },
            {
                btnPadding: 35,
                hasBg: app.gameManager.upgradeUI.turretQuality[1].isSelected,
                isDisabled: (app.gameManager.upgradeUI.turretQuality[1].costScore > app.gameManager.getAvailableScore() && !app.gameManager.upgradeUI.turretQuality[1].isSelected),
                topText: "100",
                bottomText: printNumber(app.gameManager.upgradeUI.turretQuality[1].costScore),
                stagger: false,
            }))
        screenManager.shopCont.addChild(screenManager.btn(
            "Turret quality", width, {x: (width + padding) * 2, y: app.canvas_height - 190},
            () => {
                app.gameManager.ship.projectileMaxDistance = app.gameManager.upgrades.projectileMaxDistance[2]
                app.gameManager.ship.shootDelay = app.gameManager.upgrades.shootDelay[2]
                app.gameManager.save(app.gameManager.ITEM_SHIP)
                app.gameManager.upgradeUI.turretQuality.forEach(btn => btn.isSelected = false)
                app.gameManager.upgradeUI.turretQuality[2].isSelected = true
                app.gameManager.save(app.gameManager.ITEM_UPGRADE_UI)
                initShop()
            },
            {
                btnPadding: 35,
                hasBg: app.gameManager.upgradeUI.turretQuality[2].isSelected,
                isDisabled: (app.gameManager.upgradeUI.turretQuality[2].costScore > app.gameManager.getAvailableScore() && !app.gameManager.upgradeUI.turretQuality[2].isSelected),
                topText: "250",
                bottomText: printNumber(app.gameManager.upgradeUI.turretQuality[2].costScore),
                stagger: false,
            }))

        // HIT POINTS
        screenManager.shopCont.addChild(screenManager.btn(
            "Sell", width, {x: 0, y: app.canvas_height - 80},
            () => {
                app.gameManager.ship.hp = app.gameManager.upgrades.hp[0]
                app.gameManager.save(app.gameManager.ITEM_SHIP)
                app.gameManager.upgradeUI.hp.forEach(btn => btn.isSelected = false)
                app.gameManager.upgradeUI.hp[0].isSelected = true
                app.gameManager.save(app.gameManager.ITEM_UPGRADE_UI)
                initShop()
            },
            {
                btnPadding: 35,
                hasBg: false,
                stagger: false,
            }))
        screenManager.shopCont.addChild(screenManager.btn(
            "Hit points", width, {x: width + padding, y: app.canvas_height - 80},
            () => {
                app.gameManager.ship.hp = app.gameManager.upgrades.hp[1]
                app.gameManager.save(app.gameManager.ITEM_SHIP)
                app.gameManager.upgradeUI.hp.forEach(btn => btn.isSelected = false)
                app.gameManager.upgradeUI.hp[1].isSelected = true
                app.gameManager.save(app.gameManager.ITEM_UPGRADE_UI)
                initShop()
            },
            {
                btnPadding: 35,
                hasBg: app.gameManager.upgradeUI.hp[1].isSelected,
                isDisabled: (app.gameManager.upgradeUI.hp[1].costScore > app.gameManager.getAvailableScore() && !app.gameManager.upgradeUI.hp[1].isSelected),
                topText: "4",
                bottomText: printNumber(app.gameManager.upgradeUI.hp[1].costScore),
                stagger: false,
            }))
        screenManager.shopCont.addChild(screenManager.btn(
            "Hit points", width, {x: (width + padding) * 2, y: app.canvas_height - 80},
            () => {
                app.gameManager.ship.hp = app.gameManager.upgrades.hp[2]
                app.gameManager.save(app.gameManager.ITEM_SHIP)
                app.gameManager.upgradeUI.hp.forEach(btn => btn.isSelected = false)
                app.gameManager.upgradeUI.hp[2].isSelected = true
                app.gameManager.save(app.gameManager.ITEM_UPGRADE_UI)
                initShop()
            },
            {
                btnPadding: 35,
                hasBg: app.gameManager.upgradeUI.hp[2].isSelected,
                isDisabled: (app.gameManager.upgradeUI.hp[2].costScore > app.gameManager.getAvailableScore() && !app.gameManager.upgradeUI.hp[2].isSelected),
                topText: "5",
                bottomText: printNumber(app.gameManager.upgradeUI.hp[2].costScore),
                stagger: false,
            }))
        
        // LASER AIM
        screenManager.shopCont.addChild(screenManager.btn(
            "Sell", width, {x: app.canvas_width - width - 60, y: app.canvas_height - 630},
            () => {
                app.gameManager.ship.laser = app.gameManager.upgrades.laser[0]
                app.gameManager.ship.precisionMode = app.gameManager.upgrades.precisionMode[0]
                app.gameManager.save(app.gameManager.ITEM_SHIP)
                app.gameManager.upgradeUI.gadgets.forEach(btn => btn.isSelected = false)
                app.gameManager.upgradeUI.gadgets[0].isSelected = true
                app.gameManager.save(app.gameManager.ITEM_UPGRADE_UI)
                initShop()
            },
            {
                btnPadding: 35,
                hasBg: false,
                stagger: false,
            }))
        screenManager.shopCont.addChild(screenManager.btn(
            "Laser aim", width, {x: app.canvas_width - width - 60, y: app.canvas_height - 520},
            () => {
                app.gameManager.ship.laser = app.gameManager.upgrades.laser[1]
                app.gameManager.save(app.gameManager.ITEM_SHIP)
                app.gameManager.upgradeUI.gadgets[1].isSelected = true
                app.gameManager.save(app.gameManager.ITEM_UPGRADE_UI)
                initShop()
            },
            {
                btnPadding: 35,
                hasBg: app.gameManager.upgradeUI.gadgets[1].isSelected,
                isDisabled: (app.gameManager.upgradeUI.gadgets[1].costScore > app.gameManager.getAvailableScore() && !app.gameManager.upgradeUI.gadgets[1].isSelected),
                topText: "Gadget",
                bottomText: printNumber(app.gameManager.upgradeUI.gadgets[1].costScore),
                stagger: false,
            }))
        
        // SCORE & BACK TO LEVELS
        screenManager.shopCont.addChild(screenManager.btn(
            printNumber(app.gameManager.getAvailableScore()), width, {x: app.canvas_width - width - 60, y: app.canvas_height - 190},
            () => {},
            {
                btnPadding: 35,
                stagger: false,
                topText: "Available score",
                bottomText: printNumber(app.gameManager.getScore()),
                hasFunction: false,
            }))
        
        screenManager.shopCont.addChild(screenManager.btn(
            "Back to levels", width, {x: app.canvas_width - width - 60, y: app.canvas_height - 80},
            () => {
                initLevels()
                screenManager.slideOutShop()
            },
            {
                btnPadding: 35,
                stagger: false,
            }))

        screenManager.menuCont.addChild(screenManager.shopCont)
    }

    screenManager.slideInShop = () => {
        TweenMax.to(screenManager.levelCont, 1, {x: -app.canvas_width, ease: Power2.easeInOut})
        TweenMax.to(screenManager.shopCont, 1, {x: 30, ease: Power2.easeInOut})
        TweenMax.to(screenManager.menuBgCont, 1, {x: -app.canvas_width/4, ease: Power2.easeInOut})
        screenManager.status = screenManager.STATUS_SHOP
    }

    screenManager.slideOutShop = () => {
        TweenMax.to(screenManager.levelCont, 1, {x: 30, ease: Power2.easeInOut})
        TweenMax.to(screenManager.shopCont, 1, {x: app.canvas_width + 30, ease: Power2.easeInOut})
        TweenMax.to(screenManager.menuBgCont, 1, {x: 0, ease: Power2.easeInOut})
        screenManager.status = screenManager.STATUS_MENU
    }

    screenManager.btn = (txt, width, {x = 0, y = 0}, onClick, {
            isHidden = false,
            isDisabled = false,
            btnPadding = 20,
            isGreen = false,
            hasBg = true,
            topText = "",
            bottomText = "",
            stagger = true,
            hasFunction = true,
        }) => {
        if(isHidden)
            return
            
        const btnCont = new createjs.Container()
        btnCont.x = x
        btnCont.y = y
        
        const color = isDisabled ? "#666" : isGreen ? "rgb(40, 220, 10)" : "#007acc"
        const txtObj = new createjs.Text()
        txtObj.font = "bold 24px Coda"
        txtObj.color = color
        txtObj.text = txt
        txtObj.textAlign = "center"
        txtObj.x = width/2
        txtObj.y = btnPadding
        btnCont.addChild(txtObj)
        
        const txtTop = new createjs.Text()
        txtTop.font = "bold 16px Coda"
        txtTop.color = color
        txtTop.text = topText
        txtTop.textAlign = "center"
        txtTop.x = width/2
        txtTop.y = 15
        btnCont.addChild(txtTop)
        
        const txtBottom = new createjs.Text()
        txtBottom.font = "bold 16px Coda"
        txtBottom.color = color
        txtBottom.text = bottomText
        txtBottom.textAlign = "center"
        txtBottom.x = width/2
        txtBottom.y = txtObj.getBounds().height + btnPadding + 6
        btnCont.addChild(txtBottom)

        if(!isDisabled && hasFunction) {
            const txtBg = new createjs.Shape()
            txtBg.graphics
                .f("rgb(150, 150, 255)")
                .dr(0, 0, width, txtObj.getBounds().height + 2*btnPadding)
            txtBg.alpha = hasBg ? .15 : .01
            txtBg.on("mouseover", e => {
                app.sound.playSFX("btn_hover")
                txtBg.alpha = .3
            })
            txtBg.on("mouseout", e => txtBg.alpha = hasBg ? .15 : .01)
            txtBg.mouseEnabled = true
            btnCont.addChild(txtBg)
    
            txtBg.addEventListener("click", e => {
                app.sound.playSFX("btn_select")
                stagger ? 
                    setTimeout(onClick, 500) :
                    onClick()
            })
        }

        return btnCont
    }

    screenManager.init = () => {
        screenManager.crosshairSprite = new createjs.Sprite(
            new createjs.SpriteSheet({
                images: [game.assets[`crosshair`]], 
                frames: {width: 64, height: 64, regX: 32, regY: 32}
        }))
        screenManager.crosshairSprite.z = 999
        app.stage.addChild(screenManager.crosshairSprite)

        showMenu()
    }

    screenManager.update = (deltaTime) => {
        screenManager.crosshairSprite.x = app.stage.mouseX
        screenManager.crosshairSprite.y = app.stage.mouseY

        if(screenManager.status === screenManager.STATUS_MENU) {

        }
        else if(screenManager.status === screenManager.STATUS_GAME) {
            game.update(deltaTime)
        }
        game.sortGfx(app.stage)
    }

    return screenManager

})(window.app.screenManager, window.app, window.app.game, jQuery, createjs, EasingFunctions)