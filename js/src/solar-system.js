"use strict"

window.app = window.app || {}
window.app.game = window.app.game || {}
window.app.solarSystem = window.app.solarSystem || {}

/*
 * GAME
 * */
window.app.solarSystem = ((solarSystem, game, app, createjs, EasingFunctions) => {

    const sunRadius = 165

    const
        sunRecipe = {
            gravityDistance: 500,
            gravityAcceleration: .1,
        },
        
        planetRecipe = {
            gravityDistance: 150,
            gravityAcceleration: .25,
            collisionExplosionSheet: null,
            destroyExplosionSheet: null,
        }

    solarSystem.init = ({x, y, scale}) => {
        const systemRadius = ((game.stageWidth > game.stageHeight) ? game.stageHeight*.4 : game.stageWidth*.4)
                                / scale
                                * randomInt(6, 9)/10

        console.log(systemRadius, scale, game.stageHeight, app.canvas_height)
        const systemCont = new createjs.Container()
        systemCont.x = x
        systemCont.y = y
        systemCont.scaleX = systemCont.scaleY = scale

        planetRecipe.collisionExplosionSheet = new createjs.SpriteSheet({
            images: [game.assets[`explosion_hit_planet`]], 
            frames: {width: 256, height: 256, regX: 65, regY: 128, count: 64},
            animations: {
                explode: [2, 64, null, 1]
            }
        })

        planetRecipe.destroyExplosionSheet = new createjs.SpriteSheet({
            images: [game.assets[`explosion_destroy_planet`]], 
            frames: {width: 512, height: 512, regX: 256, regY: 256, count: 64},
            animations: {
                explode: [0, 64, null, .7]
            }
        })

        const planetTypesUnique = generateArr(0, 9)
        const planetsArrNum = generateArr(1, randomInt(2, 3))
        const planetsNum = planetsArrNum.length
        const planets = planetsArrNum
            .map(i => solarSystem.createPlanet(systemCont, planetTypesUnique, {planetsNum, planetNum: i, systemRadius}))

        const sun = solarSystem.createSun(systemCont)

        planets
            .forEach(planet => {
                planet.sunRotation.x = sun.sunSprite.x
                planet.sunRotation.y = sun.sunSprite.y
                planet.shadow.rotation = deg(game.getAngle(planet.position, sun.sunSprite, true))
            })

        game.systemCont.addChild(systemCont)

        return {systemCont, sun, planets, systemRadius}
    }

    solarSystem.createSun = (systemCont) => {
        const spriteSheet = new createjs.SpriteSheet({
            images: [game.assets[`sun`]], 
            frames: {width: 330, height: 330, regX: 165, regY: 165, count: 15},
            animations: {
                shine: [0, 14, "shine", .1]
            },
            framerate: .5
        })
        const sunSprite = new createjs.Sprite(spriteSheet, "shine")
        sunSprite.width = sunSprite.height = 330
        const sunCont = new createjs.Container()
        sunCont.addChild(sunSprite)
        systemCont.addChild(sunCont)

        let hp = 8

        const getRadius = (withScale = true) =>
            (120 * sunSprite.scaleX + 120 * sunSprite.scaleY) / 2 * (withScale ? game.scale : 1)

        const getPosition = () =>
            game.getWorldPosition(sunSprite)

        const collisionExplosion = (angle, size) => {
            const sprite = new createjs.Sprite(planetRecipe.collisionExplosionSheet, "explode")
            const scale = mapRange(size, 0, 1, .5, .9)
            const planetAngle = deg(angle)
            const coords = game.polarToCartesian(getRadius(false), toRad(planetAngle))
            sprite.x = coords.x
            sprite.y = coords.y
            sprite.scaleX = sprite.scaleY = scale
            sprite.rotation = planetAngle
            sunCont.addChild(sprite)
        }

        sunSprite.cache(-165, -165, 330, 330)
        const getHit = damage => {
            instance.hp -= damage
            game.gui.addPlanetHit(damage)

            TweenMax.to({r: 1, g: 1, b: 1, rOffset: 0}, .25, {r: .5, g: .5, b: .5, rOffset: 150, repeat: 1, yoyo: true, onUpdate: function() {
                sunSprite.filters = [
                    new createjs.ColorFilter(this.target.r,this.target.g,this.target.b,1, this.target.rOffset,0,0,0)
                ]
            }})

            generateArr(1, damage)
                .forEach(() => {
                    if(instance.HPBars.length >= 1) {
                        const hpBar = instance.HPBars.pop()
                        TweenMax.to(hpBar, .75, {scaleX: .7, scaleY: .7, alpha: .2, onComplete: function() {
                            // instance.HPBarPosition.removeChild(hpBar)
                        }})
                    }
                })

            if(instance.hp <= 0 && instance._destroyAnimationPlaying !== true) {
                game.status = game.STATUS_ENDGAME
                app.screenManager.showEndgame(true, "Sun has been destroyed")
            }
        }

        let instance = {
            position: sunCont, sunCont, sunSprite,
            getRadius, getPosition,
            asteroidGroups: [],
            collisionExplosion, getHit, hp,
            gravityDistance: sunRecipe.gravityDistance, gravityAcceleration: sunRecipe.gravityAcceleration,
        }

        instance = app.game.gui.renderPlanetHitGUI(instance)
        instance.sunCont.addChild(instance.HPBarCont)
        
        return instance
    }

    solarSystem.createPlanet = (systemCont, planetTypesUnique, {planetsNum, planetNum, systemRadius, isRogue}) => {
        const planetType = planetTypesUnique.splice(randomInt(0, planetTypesUnique.length-1), 1)
        const planetRotationSpeed = (Math.random() > .5 ? -1 : 1) * randomInt(5, 80)/100
        const sunRotationSpeed = (Math.random() > .5 ? -1 : 1) * randomInt(2, 8)/100
        const distanceRowWidth = (systemRadius - sunRadius) / planetsNum
        const distanceFromSun = sunRadius + (distanceRowWidth * planetNum * randomInt(8, 12)/10)
                             
        const planetScale = randomInt(5, 10)/10
        const hp = Math.round(5 * planetScale)

        let sunRotation = new createjs.Container()
        sunRotation.rotation = Math.random() * 360

        const lineWidth = 6
        const planetCircleGUI = new createjs.Shape()
        planetCircleGUI.graphics
            .ss(lineWidth)
            .s("rgb(150, 150, 255)")
            .dc(0, 0, distanceFromSun)
        planetCircleGUI.alpha = .15
        sunRotation.addChild(planetCircleGUI)
        planetCircleGUI.cache(-distanceFromSun - lineWidth, -distanceFromSun - lineWidth, distanceFromSun*2 + lineWidth*2, distanceFromSun*2 + lineWidth*2)

        let position = new createjs.Container()
        position.x = distanceFromSun
        sunRotation.addChild(position)

        let body = new createjs.Container()
        body.scaleX = body.scaleY = planetScale
        position.addChild(body)

        let sprite = new createjs.Sprite(new createjs.SpriteSheet({
            images: [game.assets[`planet${planetType}`]], 
            frames: {width: 150, height: 150, regX: 75, regY: 75}
        }))
        sprite.width = sprite.height = 150
        body.addChild(sprite)

        let shadow = new createjs.Sprite(new createjs.SpriteSheet({
            images: [game.assets[`shadow`]], 
            frames: {width: 150, height: 150, regX: 75, regY: 75}
        }))
        shadow.alpha = mapRange(distanceFromSun, sunRadius, systemRadius, 1, .5)
        body.addChild(shadow)

        const getRadius = (withScale = true) =>
            sprite.spriteSheet._frameHeight/2 * body.scaleX * (withScale ? game.scale : 1)

        const getPosition = () =>
            game.getWorldPosition(body)

        const collisionExplosion = (angle, size) => {
            const sprite = new createjs.Sprite(planetRecipe.collisionExplosionSheet, "explode")
            const scale = mapRange(size, 0, 1, .5, .9)
            const planetAngle = deg(angle - sunRotation.rotation)
            const coords = game.polarToCartesian(getRadius(false), toRad(planetAngle))
            sprite.x = coords.x
            sprite.y = coords.y
            sprite.scaleX = sprite.scaleY = scale
            sprite.rotation = planetAngle
            position.addChild(sprite)
            app.sound.playSFX("asteroid_explosion_large")
        }

        body.cache(-75, -75, 150, 150)
        const getHit = damage => {
            instance.hp -= damage
            game.gui.addPlanetHit(damage)

            TweenMax.to({r: 1, g: 1, b: 1, rOffset: 0}, .25, {r: .6, g: .6, b: .6, rOffset: 120, repeat: 1, yoyo: true, onUpdate: function() {
                body.filters = [
                    new createjs.ColorFilter(this.target.r,this.target.g,this.target.b,1, this.target.rOffset,0,0,0)
                ]
                body.updateCache()
            }})

            TweenMax.to({r: 1, g: 1, b: 1, rOffset: 0}, .25, {r: 0, g: 0, b: 0, rOffset: 255, repeat: 1, yoyo: true, onUpdate: function() {
                planetCircleGUI.filters = [
                    new createjs.ColorFilter(this.target.r,this.target.g,this.target.b,1, this.target.rOffset,0,0,0)
                ]
                planetCircleGUI.updateCache()
            }})

            generateArr(1, damage)
                .forEach(() => {
                    if(instance.HPBars.length >= 1) {
                        const hpBar = instance.HPBars.pop()
                        TweenMax.to(hpBar, .75, {scaleX: .7, scaleY: .7, alpha: .2, onComplete: function() {
                            // instance.HPBarPosition.removeChild(hpBar)
                        }})
                    }
                })
            if(instance.hp <= 0 && instance._destroyAnimationPlaying !== true)
                instance.destroy()
        }
        
        const destroy = () => {
            const sprite = new createjs.Sprite(planetRecipe.destroyExplosionSheet, "explode")
            sprite.scaleX = sprite.scaleY = planetScale
            sprite.rotation = randomInt(0, 360)
            position.addChild(sprite)
            
            instance._destroyAnimationPlaying = true
            
            sprite.addEventListener("change", (e, type) => {
                const destroySpriteFrame = 18
                body.scaleX = body.scaleY = planetScale - EasingFunctions.easeInQuad(e.currentTarget.currentFrame / destroySpriteFrame) * planetScale*2/4
                // TweenMax.to(instance.HPBarCont, .9, {scaleX: .5, scaleY: .5, alpha: 0, ease: Power1.easeIn}) // NOT TWEENING

                if(e.currentTarget.currentFrame == destroySpriteFrame) {
                    app.sound.playSFX("big_explosion")
                    instance.asteroidGroups.forEach(group => {
                        group.asteroids.forEach(asteroid => {
                            app.asteroid.explode(game.systemCont.globalToLocal(asteroid.getPosition().x, asteroid.getPosition().y), "large", asteroid.size)
                        })
                    })
                }
                if(e.currentTarget.currentFrame >= destroySpriteFrame) {
                    position.removeChild(body)
                    position.removeChild(instance.HPBarCont)
                }                                
                if(e.currentTarget.currentFrame >= 64) {
                    sunRotation.removeChild(position)
                    instance._destroyed = true
                }
            })
        }

        let instance = {
            hp, isRogue,
            sunRotation, position, body, sprite, shadow,
            planetRotationSpeed, sunRotationSpeed, planetType, planetCircleGUI,
            getRadius, getPosition,
            asteroidGroups: [],
            gravityDistance: planetRecipe.gravityDistance, gravityAcceleration: planetRecipe.gravityAcceleration,
            collisionExplosion, getHit, destroy,
            _destroyed: false, _destroyAnimationPlaying: false
        }
        
        instance = app.game.gui.renderPlanetHitGUI(instance)
        instance.position.addChild(instance.HPBarCont)
        
        systemCont.addChild(sunRotation)
        
        return instance
    }

    solarSystem.update = deltaTime => instance => {
        let scaleX = 1 + (.02 * Math.cos(game.time/1000))
        let scaleY = 1 + (.02 * Math.sin(game.time/1000))
        instance.sun.sunSprite.scaleX = scaleX
        instance.sun.sunSprite.scaleY = scaleY
        instance.sun.sunSprite.rotation = deg(instance.sun.sunSprite.rotation + .05)
        instance.sun.sunSprite.updateCache()

        instance.planets = instance.planets.map(planet => {
            planet.sunRotation.rotation = deg(planet.sunRotation.rotation + planet.sunRotationSpeed)
            planet.sprite.rotation = deg(planet.sprite.rotation + planet.planetRotationSpeed)
            planet.HPBarCont.rotation = -planet.sunRotation.rotation
            planet.body.updateCache()
            return planet
        })
    }

    /*
    * END
    * */
    return solarSystem
})(window.app.solarSystem, window.app.game, window.app, createjs, EasingFunctions)