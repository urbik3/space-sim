
const ID = () => '_' + Math.random().toString(36).substr(2, 9)

const randomInt = (min, max) => {
    min = Math.ceil(min)
    max = Math.floor(max)
    return Math.floor(Math.random() * (max - min + 1)) + min
}

const printNumber = (num, separator = ".") => 
    num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, separator)

const mapRange = (num, in_min, in_max, out_min, out_max, limit_range = false, easing = x => x) => {
    let result = easing((num - in_min) / (in_max - in_min)) * (out_max - out_min) + out_min
    if(limit_range) {
        if(out_min < out_max) {
            result = Math.max(out_min, result)
            result = Math.min(out_max, result)
        } else {
            result = Math.max(out_max, result)
            result = Math.min(out_min, result)
        }
    }
    return result
}

const round = (num, floating = 2) =>
    Math.round(num * 10**floating) / 10**floating

const
    toRad = degree => degree*Math.PI/180,
    toDegree = rad => rad*180/Math.PI

const rad = rad =>
    rad < 0 ? Math.PI*2 + rad :
        rad > Math.PI*2 ? rad - Math.PI*2 :
            rad

const deg = deg =>
    deg < 0 ? 360 + deg :
        deg > 360 ? deg - 360 :
            deg

const radBetween = (radian, radBottom, radTop) => {
    radian = rad(radian)

    // MORE THAN 2PI
    if(radTop > Math.PI*2)
        radTop = rad(radTop)
    if(radBottom > Math.PI*2)
        radBottom = rad(radBottom)

    // LESS THAN 0
    if (radBottom < 0 || radTop < 0) {
        if (radBottom < 0 && radTop < 0) {
            radBottom = rad(radBottom)
            radTop = rad(radTop)
        }
        else if (radBottom < 0 && radTop >= 0)
            radTop = rad(radTop)
    }

    if(radBottom > radTop)
        radBottom -= Math.PI*2

    if(radian > Math.PI*2+radBottom)
        radian -= Math.PI*2

    let isBetween = radian <= radTop && radian >= radBottom

    return isBetween
}

const logTime = (fn, msTreshold = 0, disable = false) => {
    let logging = !disable,
        startTime = new Date().getMilliseconds(),
        endTime,
        time,
        return_val

    return_val = fn()

    endTime = new Date().getMilliseconds()
    time = endTime - startTime
    if(logging && time >= msTreshold)
        console.log(time)

    return return_val
}

const generateArr = (start, end, space = 1) => {
    let arr = []
    for(let x = start; x <= end; x += space)
        arr.push(x)
    return arr
}

const eq = (a) => a
const blank = () => {}