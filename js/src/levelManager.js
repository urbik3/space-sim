"use strict"

window.app = window.app || {}
window.app.levelManager = window.app.levelManager || {}
window.app.game = window.app.game || {}

/*
 * DOUBLE RANDOM
 * */
window.app.levelManager = ((levelManager, app, game, $, createjs, EasingFunctions) => {

    const
        SHOWER_SIDE_ALL = "all",
        SHOWER_SIDE_TOP = "top",
        SHOWER_SIDE_BOTTOM = "bottom",
        SHOWER_SIDE_LEFT = "left",
        SHOWER_SIDE_RIGHT = "right"

        TARGET_DESTINATION_STRAIGHT = "straight"

        levelManager.ACTION_METEOR_SHOWER = "meteor_shower"
        levelManager.ACTION_WIN = "win"

    levelManager.levelRows = 4
    levelManager.levelCols = 3

    noise.seed(Math.random())

    levelManager.actions = []

    levelManager.createMeteorShower = (startTime, endTime, frequency, targetDestination,
            {   sizes = [0, 0, 1, 1, 2, 2, 3, 3, 4, 5],
                sideOpt = SHOWER_SIDE_ALL,
                rotationDelta = 0,
                maxSpeed = 50,
                minSpeed = 10,
                isChaingun = false,
            }) => {
        let side = sideOpt
        for(let time = startTime; time <= endTime; time += frequency) {
            let coords = {x: 0, y: 0}

            if(sideOpt === SHOWER_SIDE_ALL) {
                const int = randomInt(0, 3)
                side = (int === 0) ? SHOWER_SIDE_TOP
                        : (int === 1) ? SHOWER_SIDE_BOTTOM
                        : (int === 2) ? SHOWER_SIDE_LEFT
                        : (int === 3) ? SHOWER_SIDE_RIGHT
                        : SHOWER_SIDE_TOP
            }

            const padding = 100,
                mapOffset = 79
            if(side === SHOWER_SIDE_TOP) {
                coords.y = -mapOffset
                coords.x = isChaingun
                            ? Math.abs(noise.perlin2(0, time/20)) * (game.stageWidth - padding*2) + padding
                            : randomInt(0, game.stageWidth)
            } else if(side === SHOWER_SIDE_BOTTOM) {
                coords.y = game.stageHeight + mapOffset
                coords.x = isChaingun
                            ? Math.abs(noise.perlin2(1000, time/20)) * (game.stageWidth - padding*2) + padding
                            : randomInt(0, game.stageWidth)
            } else if(side === SHOWER_SIDE_LEFT) {
                coords.y = isChaingun
                            ? Math.abs(noise.perlin2(2000, time/20)) * (game.stageHeight - padding*2) + padding
                            : randomInt(0, game.stageHeight)
                coords.x = -mapOffset
            } else if(side === SHOWER_SIDE_RIGHT) {
                coords.y = isChaingun
                            ? Math.abs(noise.perlin2(3000, time/20)*2) * (game.stageHeight - padding*2) + padding
                            : randomInt(0, game.stageHeight)
                coords.x = game.stageWidth + mapOffset
            }

            const speed = randomInt(minSpeed, maxSpeed)/100
            let angle
            if(targetDestination === TARGET_DESTINATION_STRAIGHT) {
                angle = (side === SHOWER_SIDE_TOP) ? 90 :
                            (side === SHOWER_SIDE_BOTTOM) ? 270 :
                            (side === SHOWER_SIDE_LEFT) ? 0 :
                            (side === SHOWER_SIDE_RIGHT) ? 180 : 0
            } else {
                angle = game.getAngle(coords, targetDestination, true)
            }
            angle += randomInt(-rotationDelta, rotationDelta)
            let speedVector = game.polarToCartesian(speed, toRad(angle))

            const size = sizes[randomInt(0, sizes.length - 1)]

            levelManager.actions.push([time, levelManager.ACTION_METEOR_SHOWER, "grey", size, {coords: {...coords}, speed: speedVector, scale: game.scale}])
        }
    }

    levelManager.win = time => 
        levelManager.actions.push([time, levelManager.ACTION_WIN])

    /* 
        OORT CLOUD
    */
    levelManager.config1 = levelManager.config5 = levelManager.config9 = () => ({
        width: app.canvas_width * 1.5,
        height: app.canvas_height,
        mapPadding: 300,
        scale: .8,
        music: "musicCargoship",
    })
    
    levelManager.init1 = () => {
        game.shipInstance = app.ship.create({})

        game.asteroids.push(
            ...generateArr(1, 30)
                .map(i => {
                    const asteroid = app.asteroid.createAsteroid("grey", randomInt(0, 3), {
                        scale: game.scale,
                        coords: "random",
                        speed: {x: randomInt(-4, 4)/100, y: randomInt(-4, 4)/100}
                    })
                    return asteroid
                })
                .filter(asteroid => asteroid.getPosition().x > game.stageWidth/4)
                .map(asteroid => {
                    game.systemCont.addChild(asteroid.cont)
                    return asteroid
                }))
                
            levelManager.createMeteorShower(25, 120, 2, TARGET_DESTINATION_STRAIGHT, {
                sideOpt: SHOWER_SIDE_RIGHT,
                rotationDelta: 10,
                sizes: [2, 3],
                maxSpeed: 40,
                minSpeed: 20,
            })
            
            levelManager.win(130)
    }
    
    levelManager.init5 = () => {
        game.shipInstance = app.ship.create({})

        game.asteroids.push(
            ...generateArr(1, 60)
                .map(i => {
                    const asteroid = app.asteroid.createAsteroid("grey", randomInt(0, 3), {
                        scale: game.scale,
                        coords: "random",
                        speed: {x: randomInt(-4, 4)/100, y: randomInt(-4, 4)/100}
                    })
                    return asteroid
                })
                .filter(asteroid => asteroid.getPosition().x > game.stageWidth/4)
                .map(asteroid => {
                    game.systemCont.addChild(asteroid.cont)
                    return asteroid
                }))
                
        levelManager.createMeteorShower(25, 60, 1.5, TARGET_DESTINATION_STRAIGHT, {
            sideOpt: SHOWER_SIDE_RIGHT,
            rotationDelta: 20,
            sizes: [0, 0, 0, 1, 1, 1, 2, 2, 2, 3, 3, 3, 4],
            maxSpeed: 60,
            minSpeed: 20,
        })
                
        levelManager.createMeteorShower(70, 140, 5, TARGET_DESTINATION_STRAIGHT, {
            sideOpt: SHOWER_SIDE_LEFT,
            rotationDelta: 20,
            sizes: [2, 3],
            maxSpeed: 100,
            minSpeed: 10,
        })
                
        levelManager.createMeteorShower(70, 140, .8, TARGET_DESTINATION_STRAIGHT, {
            sideOpt: SHOWER_SIDE_RIGHT,
            rotationDelta: 10,
            sizes: [0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 2, 3],
            maxSpeed: 150,
            minSpeed: 140,
            isChaingun: true,
        })
                
        levelManager.createMeteorShower(150, 200, 1.5, TARGET_DESTINATION_STRAIGHT, {
            sideOpt: SHOWER_SIDE_RIGHT,
            rotationDelta: 20,
            sizes: [2, 2, 2, 3, 3, 3, 4],
            maxSpeed: 60,
            minSpeed: 20,
        })
            
        levelManager.win(210)
    }
    
    levelManager.init9 = () => {
        game.shipInstance = app.ship.create({})

        const sizes = [0, 0, 0, 1, 1, 1, 2, 2, 2, 2, 3, 3, 3, 3, 4, 5]
        game.asteroids.push(
            ...generateArr(1, 100)
                .map(i => {
                    const asteroid = app.asteroid.createAsteroid("grey", sizes[randomInt(0, sizes.length-1)], {
                        scale: game.scale,
                        coords: "random",
                        speed: {x: randomInt(-4, 4)/100, y: randomInt(-4, 4)/100}
                    })
                    return asteroid
                })
                .filter(asteroid => asteroid.getPosition().x > game.stageWidth/4)
                .map(asteroid => {
                    game.systemCont.addChild(asteroid.cont)
                    return asteroid
                }))
                
        levelManager.createMeteorShower(40, 100, 1.5, TARGET_DESTINATION_STRAIGHT, {
            sideOpt: SHOWER_SIDE_RIGHT,
            rotationDelta: 20,
            sizes: [0, 0, 0, 1, 1, 1, 2, 2, 2, 2, 3, 3, 3, 3, 5, 5],
            maxSpeed: 60,
            minSpeed: 20,
        })
                
        levelManager.createMeteorShower(110, 200, .8, TARGET_DESTINATION_STRAIGHT, {
            sideOpt: SHOWER_SIDE_RIGHT,
            rotationDelta: 10,
            sizes: [0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 4],
            maxSpeed: 150,
            minSpeed: 140,
            isChaingun: true,
        })
                
        levelManager.createMeteorShower(210, 300, 1, TARGET_DESTINATION_STRAIGHT, {
            sideOpt: SHOWER_SIDE_RIGHT,
            rotationDelta: 20,
            sizes: [0, 0, 1, 1, 2, 2, 3, 3, 4, 5],
            maxSpeed: 150,
            minSpeed: 50,
        })

        levelManager.win(310)
    }

    /* 
        ROGUE PLANETS
    */
    levelManager.config2 = levelManager.config6 = levelManager.config10 = () => ({
        width: app.canvas_width * 1.5,
        height: app.canvas_height * 1.5,
        mapPadding: 150,
        scale: .7,
        music: "musicVoyager",
    })

    levelManager.init2 = () => {
        game.shipInstance = app.ship.create({startY: game.stageHeight * 3/4})

        game.asteroids.push(
            ...generateArr(1, 20)
                .map(i => {
                    const asteroid = app.asteroid.createAsteroid("grey", randomInt(0, 3), {
                        scale: game.scale,
                        coords: "random",
                        speed: {x: randomInt(-4, 4)/100, y: randomInt(-4, 4)/100}
                    })
                    return asteroid
                })
                .filter(asteroid => asteroid.getPosition().x > game.stageWidth/6)
                .map(asteroid => {
                    game.systemCont.addChild(asteroid.cont)
                    return asteroid
                }))

        levelManager.createMeteorShower(0, 60, 3, TARGET_DESTINATION_STRAIGHT, {
            sideOpt: SHOWER_SIDE_ALL,
            rotationDelta: 10,
            sizes: [2, 3],
            maxSpeed: 30,
            minSpeed: 10,
        })

        levelManager.createMeteorShower(60, 70, .5, TARGET_DESTINATION_STRAIGHT, {
            sideOpt: SHOWER_SIDE_ALL,
            rotationDelta: 10,
            sizes: [2, 3],
            maxSpeed: 80,
            minSpeed: 30,
        })

        levelManager.createMeteorShower(70, 140, 3, TARGET_DESTINATION_STRAIGHT, {
            sideOpt: SHOWER_SIDE_ALL,
            rotationDelta: 10,
            sizes: [0, 1, 2, 2, 2, 3, 3, 3],
            maxSpeed: 30,
            minSpeed: 10,
        })

        levelManager.createMeteorShower(130, 140, .5, TARGET_DESTINATION_STRAIGHT, {
            sideOpt: SHOWER_SIDE_ALL,
            rotationDelta: 10,
            sizes: [0, 1, 2, 2, 3, 3],
            maxSpeed: 80,
            minSpeed: 30,
        })
            
        levelManager.win(170)
    }

    levelManager.init6 = () => {
        game.shipInstance = app.ship.create({startY: game.stageHeight * 3/4})

        levelManager.createMeteorShower(0, 10, 1, TARGET_DESTINATION_STRAIGHT, {
            sideOpt: SHOWER_SIDE_ALL,
            rotationDelta: 10,
            sizes: [0, 0, 0, 1, 1, 1, 2, 2, 2, 3, 3, 3, 4],
            maxSpeed: 80,
            minSpeed: 20,
        })

        levelManager.createMeteorShower(30, 40, 1, TARGET_DESTINATION_STRAIGHT, {
            sideOpt: SHOWER_SIDE_ALL,
            rotationDelta: 10,
            sizes: [0, 0, 0, 1, 1, 1, 2, 2, 2, 3, 3, 3, 4],
            maxSpeed: 80,
            minSpeed: 20,
        })

        levelManager.createMeteorShower(60, 70, 1, TARGET_DESTINATION_STRAIGHT, {
            sideOpt: SHOWER_SIDE_ALL,
            rotationDelta: 10,
            sizes: [0, 0, 0, 1, 1, 1, 2, 2, 3, 3, 4],
            maxSpeed: 80,
            minSpeed: 20,
        })

        levelManager.createMeteorShower(90, 100, .66, TARGET_DESTINATION_STRAIGHT, {
            sideOpt: SHOWER_SIDE_ALL,
            rotationDelta: 10,
            sizes: [0, 0, 0, 1, 1, 1, 2, 2, 3, 3, 4],
            maxSpeed: 80,
            minSpeed: 20,
        })

        levelManager.createMeteorShower(120, 130, .5, TARGET_DESTINATION_STRAIGHT, {
            sideOpt: SHOWER_SIDE_ALL,
            rotationDelta: 10,
            sizes: [0, 0, 0, 1, 1, 1, 2, 2, 3, 3, 4],
            maxSpeed: 80,
            minSpeed: 20,
        })

        levelManager.win(160)
    }

    levelManager.init10 = () => {
        game.shipInstance = app.ship.create({startY: game.stageHeight * 3/4})

        levelManager.createMeteorShower(0, 10, .25, TARGET_DESTINATION_STRAIGHT, {
            sideOpt: SHOWER_SIDE_ALL,
            rotationDelta: 45,
            sizes: [0, 0, 0, 1, 1, 1, 2, 2, 2, 3, 3, 3, 4, 5],
            maxSpeed: 120,
            minSpeed: 20,
        })

        levelManager.createMeteorShower(20, 80, 1, TARGET_DESTINATION_STRAIGHT, {
            sideOpt: SHOWER_SIDE_ALL,
            rotationDelta: 45,
            sizes: [0, 0, 0, 1, 1, 1, 2, 2, 2, 3, 3, 3, 4, 5],
            maxSpeed: 80,
            minSpeed: 20,
        })

        levelManager.createMeteorShower(90, 100, .2, TARGET_DESTINATION_STRAIGHT, {
            sideOpt: SHOWER_SIDE_ALL,
            rotationDelta: 45,
            sizes: [0, 0, 0, 1, 1, 1, 2, 2, 2, 3, 3, 3, 4, 5],
            maxSpeed: 150,
            minSpeed: 80,
        })

        levelManager.createMeteorShower(120, 180, .5, TARGET_DESTINATION_STRAIGHT, {
            sideOpt: SHOWER_SIDE_ALL,
            rotationDelta: 45,
            sizes: [0, 0, 0, 1, 1, 1, 4, 5],
            maxSpeed: 200,
            minSpeed: 40,
        })

        levelManager.win(190)
    }

    /* 
        SOLAR DEFENSE
    */
    levelManager.config3 = levelManager.config7 = levelManager.config11 = () => ({
        width: app.canvas_width * 1.8,
        height: app.canvas_height * 1.8,
        mapPadding: 250,
        scale: .6,
        music: "musicPropaganda",
    })
    
    levelManager.init3 = () => {
        game.solarSystems.push(app.solarSystem.init({x: game.stageWidth/2, y: game.stageHeight/2, scale: game.scale}))

        game.shipInstance = app.ship.create({})

        levelManager.createMeteorShower(0, 10, 1, game.solarSystems[0].sun.getPosition(), {
            rotationDelta: 30,
            maxSpeed: 15,
            minSpeed: 5,
            sizes: [2, 3],
        })

        levelManager.createMeteorShower(10, 40, 3, game.solarSystems[0].sun.getPosition(), {
            rotationDelta: 30,
            maxSpeed: 5,
            minSpeed: 5,
            sizes: [2, 3],
        })

        levelManager.createMeteorShower(45, 60, 3, game.solarSystems[0].sun.getPosition(), {
            rotationDelta: 30,
            maxSpeed: 5,
            minSpeed: 5,
            sizes: [0, 1, 2, 2, 3, 3],
        })

        levelManager.createMeteorShower(70, 80, 1, game.solarSystems[0].sun.getPosition(), {
            rotationDelta: 30,
            maxSpeed: 15,
            minSpeed: 5,
            sizes: [0, 1, 2, 3],
        })

        levelManager.createMeteorShower(90, 150, 3, game.solarSystems[0].sun.getPosition(), {
            sideOpt: SHOWER_SIDE_RIGHT,
            rotationDelta: 30,
            maxSpeed: 8,
            minSpeed: 8,
            sizes: [0, 1, 2, 2, 3, 3],
        })

        levelManager.createMeteorShower(90, 150, 3, game.solarSystems[0].sun.getPosition(), {
            sideOpt: SHOWER_SIDE_LEFT,
            rotationDelta: 30,
            maxSpeed: 8,
            minSpeed: 8,
            sizes: [0, 1, 2, 2, 3, 3],
        })
            
        levelManager.win(180)
    }

    levelManager.init7 = () => {
        game.solarSystems.push(app.solarSystem.init({x: game.stageWidth/2, y: game.stageHeight/2, scale: game.scale}))

        game.shipInstance = app.ship.create({})

        levelManager.createMeteorShower(0, 60, 2, game.solarSystems[0].sun.getPosition(), {
            rotationDelta: 30,
            maxSpeed: 15,
            minSpeed: 5,
            sizes: [0, 1, 2, 3],
        })

        levelManager.createMeteorShower(80, 140, 2, game.solarSystems[0].sun.getPosition(), {
            rotationDelta: 30,
            maxSpeed: 25,
            minSpeed: 10,
            sizes: [0, 0, 0, 1, 1, 1, 2, 2, 2, 3, 3, 3, 4],
        })

        levelManager.win(200)
    }

    levelManager.init11 = () => {
        game.solarSystems.push(app.solarSystem.init({x: game.stageWidth/2, y: game.stageHeight/2, scale: game.scale}))

        game.shipInstance = app.ship.create({})

        levelManager.createMeteorShower(0, 60, 1.5, game.solarSystems[0].sun.getPosition(), {
            rotationDelta: 30,
            maxSpeed: 15,
            minSpeed: 5,
            sizes: [0, 1, 2, 3],
        })

        levelManager.createMeteorShower(85, 145, 1.5, game.solarSystems[0].sun.getPosition(), {
            rotationDelta: 30,
            maxSpeed: 25,
            minSpeed: 10,
            sizes: [0, 0, 0, 1, 1, 1, 2, 2, 2, 3, 3, 3, 4, 5, 5],
        })

        levelManager.createMeteorShower(170, 230, 1, game.solarSystems[0].sun.getPosition(), {
            rotationDelta: 30,
            maxSpeed: 25,
            minSpeed: 10,
            sizes: [0, 0, 0, 1, 1, 1, 2, 2, 2, 3, 3, 3, 4, 5, 5],
        })

        levelManager.win(250)
    }

    /* 
        BLACK HOLES
    */
    levelManager.config4 = levelManager.config8 = levelManager.config12 = () => ({
        width: app.canvas_width * 1.5,
        height: app.canvas_height * 1.5,
        mapPadding: 250,
        scale: .8,
        music: "musicBattleInTheMarket",
    })
    
    levelManager.init4 = () => {
        game.blackHoles.push(app.blackHole.init({x: 150, y: 150}))
        game.blackHoles.push(app.blackHole.init({x: game.stageWidth - 500, y: 300}))
        game.blackHoles.push(app.blackHole.init({x: game.stageWidth*.55, y: game.stageHeight*.75}))
        
        game.shipInstance = app.ship.create({})
                
        levelManager.createMeteorShower(0, 150, 3.5, TARGET_DESTINATION_STRAIGHT, {
            rotationDelta: 60,
            maxSpeed: 40,
            minSpeed: 10,
            sizes: [0, 0, 1, 1, 2, 2, 2, 2, 2, 3, 3, 3, 3, 3, 4],
        })
            
        levelManager.win(170)
    }

    levelManager.init8 = () => {
        game.blackHoles.push(app.blackHole.init({x: 150, y: 150}))
        game.blackHoles.push(app.blackHole.init({x: game.stageWidth - 500, y: 300}))
        game.blackHoles.push(app.blackHole.init({x: game.stageWidth*.55, y: game.stageHeight*.75}))
        game.blackHoles.push(app.blackHole.init({x: 280, y: game.stageHeight*.88}))

        game.shipInstance = app.ship.create({})

        levelManager.createMeteorShower(0, 150, 1, TARGET_DESTINATION_STRAIGHT, {
            sideOpt: SHOWER_SIDE_ALL,
            rotationDelta: 60,
            sizes: [0, 0, 0, 1, 1, 1, 2, 2, 2, 3, 3, 3, 4],
            maxSpeed: 80,
            minSpeed: 20,
        })
            
        levelManager.win(165)
    }

    levelManager.init12 = () => {
        game.blackHoles.push(app.blackHole.init({x: 150, y: 150}))
        game.blackHoles.push(app.blackHole.init({x: game.stageWidth - 500, y: 300}))
        game.blackHoles.push(app.blackHole.init({x: game.stageWidth*.55, y: game.stageHeight*.75}))
        game.blackHoles.push(app.blackHole.init({x: 280, y: game.stageHeight*.88}))
        game.blackHoles.push(app.blackHole.init({x: game.stageWidth*.85, y: game.stageHeight*.60}))
        
        game.shipInstance = app.ship.create({})

        levelManager.createMeteorShower(0, 60, 1, TARGET_DESTINATION_STRAIGHT, {
            sideOpt: SHOWER_SIDE_ALL,
            rotationDelta: 60,
            sizes: [0, 0, 1, 1, 2, 3, 4, 5, 5],
            maxSpeed: 250,
            minSpeed: 50,
        })

        levelManager.createMeteorShower(60, 120, .7, TARGET_DESTINATION_STRAIGHT, {
            sideOpt: SHOWER_SIDE_ALL,
            rotationDelta: 60,
            sizes: [0, 0, 1, 1, 2, 3, 4, 5, 5],
            maxSpeed: 250,
            minSpeed: 50,
        })

        levelManager.createMeteorShower(135, 165, .3, TARGET_DESTINATION_STRAIGHT, {
            sideOpt: SHOWER_SIDE_ALL,
            rotationDelta: 60,
            sizes: [0, 0, 1, 1, 2, 3, 4, 5, 5],
            maxSpeed: 250,
            minSpeed: 80,
        })

        levelManager.win(175)
    }

    return levelManager

})(window.app.levelManager, window.app, window.app.game, jQuery, createjs, EasingFunctions)