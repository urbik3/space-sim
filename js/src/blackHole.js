"use strict"

window.app = window.app || {}
window.app.game = window.app.game || {}
window.app.blackHole = window.app.blackHole || {}

/*
 * GAME
 * */
window.app.blackHole = ((blackHole, game, app, createjs, EasingFunctions) => {

    const gravityDistance = 400
    const gravityAcceleration = .6

    blackHole.init = (coords = false) => {
        const cont = new createjs.Container()
        if(coords) {
            cont.x = coords.x
            cont.y = coords.y
        } else {
            cont.x = randomInt(0, game.stageWidth)
            cont.y = randomInt(0, game.stageHeight)
        }
        let sprite = new createjs.Sprite(new createjs.SpriteSheet({
            images: [game.assets[`whirl`]], 
            frames: {width: 285, height: 280, regX: 142.5, regY: 140}
        }))
        cont.addChild(sprite)
        game.systemCont.addChild(cont)

        // const shape = new createjs.Shape()
        // shape.graphics
        // .f("rgba(255, 0, 0, .2")
        // .dc(0, 0, gravityDistance)
        // cont.addChild(shape)

        const getPosition = () =>
            game.getWorldPosition(cont)

        const getRadius = () => 1

        return {
            sprite,
            getPosition,
            getRadius,
            gravityDistance,
            gravityAcceleration,
        }
    }

    blackHole.update = (instance) => {
        instance.sprite.rotation += .25
    }

    return blackHole
})(window.app.blackHole, window.app.game, window.app, createjs, EasingFunctions)