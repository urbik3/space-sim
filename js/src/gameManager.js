"use strict"

window.app = window.app || {}
window.app.gameManager = window.app.gameManager || {}
window.app.game = window.app.game || {}

/*
 * GAME DATA MANAGER
 * */
window.app.gameManager = ((gameManager, app, game, $, createjs, EasingFunctions) => {

    const storage = window.localStorage

    gameManager.ITEM_GAME = "game"
    gameManager.ITEM_SHIP = "ship"
    gameManager.ITEM_UPGRADE_UI = "upgradeUI"

    gameManager.game
    gameManager.ship

    app.addInitCallback(() => {
        gameManager.game = storage.getItem(gameManager.ITEM_GAME)
        if(gameManager.game) {
            gameManager.game = JSON.parse(gameManager.game)
        } else {
            gameManager.game = {
                levels: [
                    {available: true, won: false, score: 0},
                    {available: false, won: false, score: 0},
                    {available: false, won: false, score: 0},
                    {available: false, won: false, score: 0},
                    {available: false, won: false, score: 0},
                    {available: false, won: false, score: 0},
                    {available: false, won: false, score: 0},
                    {available: false, won: false, score: 0},
                    {available: false, won: false, score: 0},
                    {available: false, won: false, score: 0},
                    {available: false, won: false, score: 0},
                    {available: false, won: false, score: 0},
                ]
            }
        }
        
        gameManager.ship = storage.getItem(gameManager.ITEM_SHIP)
        if(gameManager.ship) {
            gameManager.ship = JSON.parse(gameManager.ship)
        } else {
            gameManager.ship = {
                speedMax:               upgrades.speedMax[0],
                speedAcc:               upgrades.speedAcc[0],
                shipRotationAcc:        upgrades.shipRotationAcc[0],
                shipRotationSpeed:      upgrades.shipRotationSpeed[0],
                turretRotation:         upgrades.turretRotation[0],
                projectileMaxDistance:  upgrades.projectileMaxDistance[0],
                shootDelay:             upgrades.shootDelay[0],
                hp:                     upgrades.hp[0],
                laser:                  upgrades.laser[0],
                precisionMode:          upgrades.precisionMode[0],
            }
        }
        
        gameManager.upgradeUI = storage.getItem(gameManager.ITEM_UPGRADE_UI)
        if(gameManager.upgradeUI) {
            gameManager.upgradeUI = JSON.parse(gameManager.upgradeUI)
        } else {
            gameManager.upgradeUI = {
                jetSpeed: [{
                    isSelected: true,
                    costScore: 0,
                }, {
                    isSelected: false,
                    costScore: 10000,
                }, {
                    isSelected: false,
                    costScore: 35000,
                }], 
                rotationSpeed: [{
                    isSelected: true,
                    costScore: 0,
                }, {
                    isSelected: false,
                    costScore: 15000,
                }, {
                    isSelected: false,
                    costScore: 45000,
                }], 
                turretRotation: [{
                    isSelected: true,
                    costScore: 0,
                }, {
                    isSelected: false,
                    costScore: 20000,
                }, {
                    isSelected: false,
                    costScore: 60000,
                }], 
                turretQuality: [{
                    isSelected: true,
                    costScore: 0,
                }, {
                    isSelected: false,
                    costScore: 25000,
                }, {
                    isSelected: false,
                    costScore: 85000,
                }], 
                hp: [{
                    isSelected: true,
                    costScore: 0,
                }, {
                    isSelected: false,
                    costScore: 20000,
                }, {
                    isSelected: false,
                    costScore: 60000,
                }], 
                gadgets: [{
                    isSelected: true,
                    costScore: 0,
                }, {
                    isSelected: false,
                    costScore: 60000,
                }, {
                    isSelected: false,
                    costScore: 200000,
                }], 
            }
        }
    })

    gameManager.getScore = () => 
        gameManager.game.levels.reduce((num, val) => num + val.score, 0)

    gameManager.getAvailableScore = () => {
        const usedScore = Object.values(gameManager.upgradeUI).reduce((sum, section) =>
            sum + section.map(item => (item.isSelected) ? item.costScore : 0).reduce((cost, val) => cost + val, 0)
        , 0)

        return gameManager.getScore() - usedScore
    }

    const upgrades = gameManager.upgrades = {
        speedMax: [2, 4, 7],
        speedAcc: [.075, .1, .15],
        shipRotationAcc: [.01, .02, .05],
        shipRotationSpeed: [1, 2, 5],
        turretRotation: [0, 20, 60],
        projectileMaxDistance: [50, 100, 200],
        shootDelay: [.5, .35, .1],
        hp: [3, 4, 5],
        laser: [false, true],
        precisionMode: [false, true],
    }

    gameManager.save = (type = gameManager.ITEM_GAME) =>
        storage.setItem(type, JSON.stringify(gameManager[type]))

    return gameManager

})(window.app.gameManager, window.app, window.app.game, jQuery, createjs, EasingFunctions)