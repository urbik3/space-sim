"use strict"

window.app = window.app || {}
window.app.game = window.app.game || {}
window.app.game.gui = window.app.game.gui || {}

/*
 * DOUBLE RANDOM
 * */
window.app.game.gui = ((gui, game, app, $, createjs, EasingFunctions) => {

    gui.asteroidsDestroyed = 0
    gui.lootCollected = 0
    gui.planetHits = 0

    gui.config = (()=>{
        let obj = {
            padding: 20,
            width: 200,
            height: 20,
            strokeWidth: 2,
            
            planetHPBarHeight: 4,
            planetHPBarWidth: 20,
            planetHPBarPadding: 6,
        }
        obj.widthHalf = obj.width/2-obj.padding/2
        return obj
    })()

    gui.init = () => {
        let score = new createjs.Container()
        gui.score = score
        score.x = gui.config.padding
        score.y = app.canvas_height-gui.config.padding
        game.stage.addChild(score)

        // planetHits()
        gui.renderLootGUI()
        gui.renderAsteroidDestroyedGUI()
    }

    gui.resize = () => {
        gui.gfx.x = gui.config.padding
        gui.gfx.y = app.canvas_height-gui.config.padding
    }

    gui.update = (deltaTime) => {
    }

    gui.reset = () => {
        gui.planetHits = gui.lootCollected = gui.asteroidsDestroyed = 0
    }

    gui.getScore = () => 
        Math.max(0, gui.asteroidsDestroyed * 100 + gui.lootCollected * 1000 - gui.planetHits * 1000 + (gui.shipHP - gui.shipMaxHP) * 1000)

    const planetHits = () => {
        let text = new createjs.Text("Planet hits: ", "20px Arial", "#0077ff")
        text.textBaseline = "bottom"
        gui.score.addChild(text)

        let planetHits = new createjs.Text("0", "bold 20px Arial", "#0077ff")
        planetHits.textBaseline = "bottom"
        planetHits.x = text.getBounds().width
        gui.score.addChild(planetHits)
        gui.planetHits = planetHits
    }

    gui.addPlanetHit = (num = 1) => {
        gui.planetHits += num
        gui.planetHits.text = gui.planetHits
    }

    gui.addLoot = (num = 1) => {
        gui.lootCollected += num
        gui.asteroidLootText.text = gui.lootCollected
    }

    gui.addAsteroidDestroyed = (num = 1) => {
        gui.asteroidsDestroyed += num
        gui.asteroidDestroyedText.text = gui.asteroidsDestroyed
    }

    // LOOT GUI
    gui.renderLootGUI = () => {
        const LootCont = new createjs.Container()
        LootCont.x = 18
        LootCont.y = app.canvas_height - 95

        const LootBG = new createjs.Shape()
        LootBG.graphics
            .f("rgba(150, 150, 255, .15)")
            .dr(0, 0, 100, 50)

        const lootSprite = new createjs.Sprite(new createjs.SpriteSheet({
            images: [game.assets[`asteroid_loot`]], 
            frames: {width: 317, height: 170, regX: 158.5, regY: 85}
        }))
        lootSprite.scaleX = lootSprite.scaleY = .2
        lootSprite.y = 25
        lootSprite.x = 25
        
        const text = new createjs.Text("0", "20px Arial", "#0077ff")
        text.x = 55
        text.y = 17
        gui.asteroidLootText = text

        LootCont.addChild(LootBG, lootSprite, text)
        app.game.stage.addChild(LootCont)
    }

    // LOOT GUI
    gui.renderAsteroidDestroyedGUI = () => {
        const asteroidCont = new createjs.Container()
        asteroidCont.x = 18
        asteroidCont.y = app.canvas_height - 152

        const asteroidBG = new createjs.Shape()
        asteroidBG.graphics
            .f("rgba(150, 150, 255, .15)")
            .dr(0, 0, 100, 50)

        const asteroidSprite = new createjs.Sprite(new createjs.SpriteSheet({
            images: [game.assets[`asteroid_grey_4`]], 
            frames: {width: 58, height: 57, regX: 28.5, regY: 29}
        }))
        asteroidSprite.scaleX = asteroidSprite.scaleY = .8
        asteroidSprite.y = 25
        asteroidSprite.x = 25
        
        const text = new createjs.Text("0", "20px Arial", "#0077ff")
        text.x = 55
        text.y = 17
        gui.asteroidDestroyedText = text

        asteroidCont.addChild(asteroidBG, asteroidSprite, text)
        app.game.stage.addChild(asteroidCont)
    }
    
    // OBJECT HIT
    gui.renderPlanetHitGUI = planet => {
        const bgPadding = 6
        const yPadding = -(20 + (planet.getRadius(false)))

        return renderHitGUI(planet, bgPadding, yPadding)
    }

    gui.renderShipHitGUI = ship => {
        const bgPadding = 6
        const yPadding = app.canvas_height - 30

        return renderHitGUI(ship, bgPadding, yPadding)
    }

    gui.shipMaxHP = 0
    gui.shipHP = 0
    gui.setShipMaxHP = maxHP => gui.shipMaxHP = maxHP
    gui.setShipHP = hp => gui.shipHP = hp

    renderHitGUI = (object, bgPadding, yPadding) => {
        gui.config.planetHPBarHeight
        gui.config.planetHPBarWidth
        gui.config.planetHPBarPadding
        
        const
            HPBarCont = new createjs.Container(),
            HPBarPosition = new createjs.Container()
        HPBarPosition.x = - (object.hp * gui.config.planetHPBarWidth + gui.config.planetHPBarPadding) / 2
        HPBarPosition.y = yPadding

        const HPBarBg = new createjs.Shape()
        HPBarBg.graphics
            .f("rgba(150, 150, 255, .15)")
            .dr(- (object.hp * (gui.config.planetHPBarWidth + gui.config.planetHPBarPadding) / 2)
                    - gui.config.planetHPBarPadding - bgPadding,
                - (gui.config.planetHPBarHeight/2 + bgPadding) + yPadding,
                object.hp * (gui.config.planetHPBarWidth + gui.config.planetHPBarPadding)
                    + gui.config.planetHPBarPadding + bgPadding*3,
                gui.config.planetHPBarHeight + bgPadding*2)

        HPBarCont.addChild(HPBarBg)
        HPBarCont.addChild(HPBarPosition)
        
        const HPBars = generateArr(0, object.hp-1)
            .map((i) => {
                const bar = new createjs.Shape()
                bar.graphics.f("rgba(40, 220, 10, 1)")
                    .dr(-(gui.config.planetHPBarWidth/2),
                    -(gui.config.planetHPBarHeight/2),
                    gui.config.planetHPBarWidth,
                    gui.config.planetHPBarHeight)
                bar.x = i * (gui.config.planetHPBarWidth + gui.config.planetHPBarPadding)
                bar.cache(-(gui.config.planetHPBarWidth/2),
                    -(gui.config.planetHPBarHeight/2),
                    gui.config.planetHPBarWidth,
                    gui.config.planetHPBarHeight)
                HPBarPosition.addChild(bar)
                return bar
            })
            
        object = {...object, HPBarCont, HPBarPosition, HPBars, HPBarBg}

        return object
    }

    /*
    * END
    * */

    return gui

})(window.app.game.gui, window.app.game, window.app, jQuery, createjs, EasingFunctions)